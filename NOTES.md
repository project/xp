Export and import configuration
-------------------------------

    # Uninstall the demo module to prevent it from being exported as a feature.
    drush pmu -y xp_block_demo

    # Enable block modules.
    drush en xp_block xp_block_field xp_block_paragraphs

    # Export block modules.
    drush features-export -y xp
    drush features-export -y xp_block
    drush features-export -y xp_block_field
    drush features-export -y xp_block_paragraphs

    # Import block modules.
    drush features-import -y xp
    drush features-import -y xp_block
    drush features-import -y xp_block_field
    drush features-import -y xp_block_paragraphs

    # Export and import demo block module.
    drush features-export -y xp_demo_block
    drush features-import -y xp_demo_block

Testing
-------

## Running PHPUnit tests.

@see [PHPUnit in Drupal](https://www.drupal.org/docs/automated-testing/phpunit-in-drupal)

    # Setup tests.
    export SIMPLETEST_DB=mysql://drupal:drupal@localhost/drupal;
    export SIMPLETEST_BASE_URL='http://localhost';
    export SYMFONY_DEPRECATIONS_HELPER='disabled';
    export BROWSERTEST_OUTPUT_DIRECTORY='DRUPAL_ROOT/sites/default/files/simpletest'
    pkill chromedriver
    chromedriver --port=4444 &

    # Execute tests.
    php ../../vendor/phpunit/phpunit/phpunit\
        --printer="\Drupal\Tests\Listeners\HtmlOutputPrinter"\
        {MODULE_PATH}

Review code
-----------

[PHP](https://www.drupal.org/node/1587138)

    # Check Drupal PHP coding standards and best practices.
    phpcs .

    # Show sniff codes in all reports.
    phpcs -s .

[JavaScript](https://www.drupal.org/node/2873849)

    # Install Eslint. (One-time)
    cd DRUPAL_ROOT/core
    yarn install

    # Check Drupal JavaScript (ES5) legacy coding standards.
    cd DRUPAL_ROOT
    core/node_modules/.bin/eslint --no-eslintrc -c=core/.eslintrc.legacy.json --ext=.js modules/sandbox/xp

[CSS](https://www.drupal.org/node/3041002)

    # Install Eslint. (One-time)
    cd DRUPAL_ROOT/core
    yarn install

    # cd DRUPAL_ROOT/core
    # yarn run lint:css ../modules/sandbox/xp --fix

Release code
------------


[Git Release Notes for Drush](https://www.drupal.org/project/grn)

    drush release-notes --nouser 1.0.0-alpha1 1.0.x


6. Tag and create a new release
-------------------------------

[Tag a release](https://www.drupal.org/node/1066342)

    git checkout 1.0.x
    git up
    git tag 1.0.0-alpha1
    git push --tags
    git push origin tag 1.0.0-alpha1

