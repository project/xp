# External Personalization (XP) module

## About

The External Personalization (XP) module provides a recipe for authoring personalized content in Drupal while relying on an external personalization service to handle all personalization decisions and business logic.

> Drupal provides the authoring experience for personalized content, and an external application handles the user tracking and decisions for displaying the personalized content.

This module’s approach is to leverage Drupal’s Block system with the Paragraphs module to create custom personalized blocks called “XP blocks.” “XP blocks” are “custom blocks” specifically intended to handle personalization. “XP blocks” are managed using a dedicated “XP block library” page. “XP blocks” may contain multiple variants of simple HTML, other blocks, and even paragraphs. Each variant has a unique ID that is shared with the external personalization engine. JavaScript sends the personalization ID to the external personalization decision engine. The personalization decision engine executes rules and returns a decision, the selected variant to be displayed to the site visitor.

## Assumptions

> One fundamental assumption is that personalization decisions are applied using client-side JavaScript.

The external personalization engine’s rules need to support defining context (i.e., homepage, landing page, blog post, etc.) and personalization ID with a list of possible variant ids. Variant IDs generally can be named after user segments (i.e., new user, returning user, authenticate user, etc.).

## Challenges

Besides the immediate challenge of integrating an external personalization decision engine into Drupal to create engaging user experiences, creating and managing personalized content introduces various challenges.

The challenges that this module addresses include...

- Providing a centralized place to track all personalized content.
- Creating a flexible and customizable mechanism for authoring personalized content.
- Easy to use tools to preview personalized content.
- Establishing best practices for integrating external personalization engines into Drupal.

## Demos

This module includes an External Personalization (XP) Block Demo module that provides working examples of personalized blocks. The Bartik theme is required to install the demo.

![External Pesonalization (XP) Demo](https://www.drupal.org/files/issues/2021-09-28/xp-demo-min.gif)

## Related/Similar Modules

What makes this module different from other personalization-related Drupal modules, is this module assumes all segmentation, business rules, and decisions are handled outside of Drupal. This module allows Drupal to do what it does best, author content, while allowing organizations to integrate with best-in-class personalization decision engines powered by enterprise CRMs and CDPs.

Below are some personalization modules that provide different levels of integrations with Drupal 8/9.

- [Smart Content](https://www.drupal.org/project/smart_content)
  Smart content is a toolset to enable real-time, anonymous website personalization on any Drupal 8 website.

- [Acquia Lift](https://www.drupal.org/project/acquia_lift)
  Acquia Lift merges content and customer data into one tool, empowering organizations to deliver the most cohesive and personalized experiences across multiple channels and devices.

## Recommended modules

- [Block Content Permissions](https://www.drupal.org/project/block_content_permissions)
  Allows you to control access to administer block content types (custom block types), administer block content (custom block library), and create, update, or delete specific types of block content.

- [Block Content Revision UI](https://www.drupal.org/project/block_content_revision_ui)
  Adds Revision UI to Block Content entities.

## Roadmap

The External Personalization (XP) module is a recipe mainly containing exported block and paragraph configuration files with a few alter hooks and a little JavaScript providing the integration with external personalization engines.

Maintaining a stable release of this module’s configuration may prove impossible. For now, this module will remain in alpha releases until an update path between releases can be determined and provided.

## Fork and go

As stated, the External Personalization (XP) module provides a recipe for building and managing personalized user experiences using an external personalization engine. This module consists primarily of default configuration with some glue code and basic APs. Experienced Drupal developers should consider the possibility of forking this module to build the best integration with their particular personalization engine and client requirements.

# Support and maintenance

> The External Personalization (XP) module is open source, but it is not free code; it is shared code.

All initial work for this module was sponsored. All additional features and support requests should be sponsored. Personalization is typically a paid service that helps businesses engage with their consumers. Therefore any work to integrate Drupal with a personalization service should be sponsored by a client or vendor. Please get in touch with the maintainer for paid support and maintenance.
