/**
 * @file
 * Reusable singleton helper methods for External Personalization module.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.Xp = {};

  /* ************************************************************************ */
  // Item methods.
  /* ************************************************************************ */

  /**
   * Get XP item element.
   *
   * @param {Object} item
   *   Item containing type and id properties.
   *
   * @return {*|jQuery|HTMLElement}
   *   The XP item element.
   */
  Drupal.Xp.getItemElement = function (item) {
    var selector = '[data-xp="' + $.escapeSelector(item.type) + '"]' +
      '[data-xp-id="' + $.escapeSelector(item.id) + '"]';
    return $(selector);
  };

  /**
   * Get XP items array from an HTML element (context).
   *
   * @param {HTMLElement} context
   *   An HTML element.
   *
   * @return {Array}
   *   An array of item objects which contain type, and id properties.
   */
  Drupal.Xp.getItems = function (context) {
    var $items = $('[data-xp]', context);
    var items = [];
    $items.each(function () {
      var item = {
        type: $(this).data('xp') || 'custom',
        id: $(this).data('xp-id')
      };
      items.push(item);
    });
    return items;
  };

  /* ************************************************************************ */
  // Variant methods.
  /* ************************************************************************ */

  /**
   * Get variant elements for an item.
   *
   * @param {Object} item
   *   Item containing type, id, and variant properties.
   *
   * @return {*|jQuery|HTMLElement}
   *   Variant HTML element.
   */
  Drupal.Xp.getVariantElement = function (item) {
    var $item = Drupal.Xp.getItemElement(item);
    var variants = (Array.isArray(item.variant)) ? item.variant : [item.variant];
    var selectors = [];
    variants.forEach(function (variant) {
      selectors.push('[data-xp-variant="' + variant + '"]');
    });
    return $item.find(selectors.join(','));
  };

  /**
   * Get variant elements for an item.
   *
   * @param {Object} item
   *   Item containing type, and id properties.
   *
   * @return {*|jQuery|HTMLElement}
   *   Variant HTML elements.
   */
  Drupal.Xp.getVariantElements = function (item) {
    var $item = Drupal.Xp.getItemElement(item);
    return $item.find('[data-xp-variant]');
  };

  /**
   * Get variant ids for an item.
   *
   * @param {Object} item
   *   Item containing type, and id properties.
   *
   * @return {Array}
   *   Variant IDs.
   */
  Drupal.Xp.getVariantsIds = function (item) {
    var $variants = Drupal.Xp.getVariantElements(item);
    var ids = [];
    $variants.each(function () {
      ids.push($(this).data('xp-variant'));
    });
    return ids;
  };

  /**
   * Show an item's variant.
   *
   * @param {Object} item
   *   Item containing type, id, and variant properties.
   * @param {Function} callback
   *   Success callback.
   *
   * @return {boolean}
   *   TRUE if the variant is found and displayed.
   */
  Drupal.Xp.showVariant = function (item, callback) {
    var $variant = Drupal.Xp.getVariantElement(item);
    if (!$variant.length) {
      return false;
    }

    var url = $variant.data('xp-variant-url');
    if (url && Drupal.ajax) {
      var settings = {
        // Append the item as query string parameters to the URL.
        url: url + '?' + $.param(item),
        // Add success callback to get the variant's HTML and show the variant.
        success: function (response, status) {
          // Call the override prototype success function to show the
          // variant's HTML.
          // @see \Drupal\xp\Controller\XpEntityController::variant
          Drupal.Ajax.prototype.success.call(this, response, status);
          // Remove the 'data-xp-variant-url' attribute, so that we can
          // call show variant without Ajax.
          $variant.removeData('xp-variant-url')
            .removeAttr('data-xp-variant-url');
          Drupal.Xp.showVariant(item, callback);
        }
      };
      Drupal.ajax(settings).execute();
    }
    else {
      var $variants = Drupal.Xp.getVariantElements(item);
      $variants.hide();
      $variant.show();
      if (callback) {
        callback();
      }
    }

    return true;
  };

}(jQuery, Drupal));
