/* eslint-disable max-nested-callbacks */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.xpTrigger = {
    attach: function (context) {
      $('[data-xp-trigger]', context).once('xp-trigger').each(function () {
        var $trigger = $(this);
        $trigger.on('click', function (event) {
          var item = $trigger.data('xp-trigger');

          var triggered = Drupal.Xp.showVariant(item, function () {
            var $element = Drupal.Xp.getVariantElement(item);

            // Display variant and highlight it,
            $element
              .css({opacity: '0.2'})
              .animate({opacity: '1'}, 1000)

            // Scroll the variant into view.
            if (!isInViewport($element)) {
              $element.get(0).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
              });
            }

            // Update ?_variant= query string parameter.
            if (Drupal.XpTest) {
              Drupal.XpTest.updateHistory(item);
            }
          });

          // If show variant is triggered then prevent the default href
          // from being triggered.
          if (triggered) {
            event.preventDefault();
          }
        });
      });
    }
  };

  /**
   * Determine if an element is visible in the view post.
   *
   * @param {*|jQuery|HTMLElement} $element
   *   An element.
   *
   * @returns {boolean}
   *   TRUE if the element is visible in the view post.
   *
   * @see https://stackoverflow.com/questions/20791374/jquery-check-if-element-is-visible-in-viewport
   */
  function isInViewport($element) {
    var elementTop = $element.offset().top;
    var elementBottom = elementTop + $element.outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  }

}(jQuery, Drupal));
