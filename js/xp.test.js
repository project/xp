(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Console styling.
   *
   * @type {string}
   */
  var consoleStyle = 'background: #ffc; color: #ooo; padding: 5px';

  Drupal.behaviors.xpTest = {
    attach: function (context) {
      var items = Drupal.Xp.getItems(context);
      if (!items.length) {
        return;
      }

      items.forEach(function (item) {
        console.log('%c Found external personalization: ' + Object.values(item).join('/'), consoleStyle);
        Drupal.Xp.getVariantsIds(item).forEach(function (variant) {
          var url = window.location.href.split('?')[0];
          console.log('%c Test \'' + variant + '\' variant: ' + url + '?_variant=' + encodeURIComponent(variant), consoleStyle);
        });
      });

      var url = drupalSettings.path.baseUrl + 'xp/test';
      var data = {data: items, location: window.location.toString()};
      $.get(url, data, function getResponse(response) {
        // Items are returned with the 'variant' property.
        response.forEach(function processResponseItem(item) {
          if (Drupal.Xp.showVariant(item)) {
            console.log('%c Displaying external personalization variant: ' + Object.values(item).join('/'), 'background: #ffc; color: #ooo; padding: 5px');
          }
        });
      });
    }
  };

  Drupal.XpTest = {};

  /**
   * Set the browser's ?_variant= query string parameter.
   *
   * @param {Object} item
   *   Item containing type, id, and variant properties.
   */
  Drupal.XpTest.updateHistory = function (item) {
    var search = window.location.search;

    // Remove the _variant query string parameter.
    search = search.replace(/[?&]_variant=[^&]+/, '').replace(/^&/, '?');

    // Append the _variant query string parameter.
    search += (search ? '&' : '?') +
      '_variant=' + encodeURIComponent(item.variant);

    // Update the browser's history.
    window.history.replaceState('', '', search);
  };

}(jQuery, Drupal, drupalSettings));
