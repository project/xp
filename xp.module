<?php

/**
 * @file
 * Provide a block where site authors can personalize the displayed.
 */

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function xp_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof EntityDeleteForm) {
    _xp_form_entity_delete_form_alter($form, $form_state);
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function _xp_form_entity_delete_form_alter(&$form, FormStateInterface $form_state) {
  $entity = $form_state->getFormObject()->getEntity();
  if (!$entity instanceof ConfigEntityInterface) {
    return;
  }

  if (!preg_match('/(\.field_xp|\.xp_|^xp_)/', $entity->id())) {
    return;
  }

  $t_args = [
    '@entity-type' => $entity->getEntityType()->getSingularLabel(),
    '@label' => $entity->label(),
    '%label' => $entity->label(),
  ];
  \Drupal::messenger()->addWarning(t('The @entity-type %label is required by the External Personalization (XP) module. <strong>Deleting this @entity-type can cause unexpected errors and issues.</strong>', $t_args));

  if (isset($form['actions'])) {
    $form['confirm'] = [
      '#type' => 'checkbox',
      '#title' => t('Yes, I want to delete the @label @entity-type', $t_args),
      '#return_value' => TRUE,
      '#required' => TRUE,
      '#weight' => 99,
    ];
    $form['actions']['#weight'] = 100;
  }
}
