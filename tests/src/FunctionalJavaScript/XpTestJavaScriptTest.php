<?php

namespace Drupal\Tests\xp\FunctionalJavaScript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests External Personalization JavaScript Drupal behaviors.
 *
 * @see js/xp.js
 * @see js/xp.test.js
 * @see \Drupal\xp_test_element\Controller\XpTestElementController
 *
 * @group xp
 */
class XpTestJavaScriptTest extends WebDriverTestBase {

  /**
   * Set default theme to stable.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_test_element'];

  /**
   * Test JavaScript.
   */
  public function testJavaScript() {
    $assert = $this->assertSession();

    // Confirm that xp.test.js is NOT added to the page.
    $this->drupalGet('/xp/test/element');
    $assert->responseNotContains('xp.test.js');
    $this->drupalGet('/xp/test');
    $assert->responseContains('You are not authorized to access this page.');

    // Create and login XP test user.
    $tester = $this->createUser(['test xp']);
    $this->drupalLogin($tester);

    // Confirm that xp.test.js is added to the page for the 'XP test user'.
    $this->drupalGet('/xp/test/element');
    $assert->responseContains('xp.test.js');

    // Check that no letter or number variants are displayed.
    $this->drupalGet('/xp/test/element');
    $assert->elementNotExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextNotContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    $assert->pageTextNotContains('The number 0');
    $assert->pageTextNotContains('The number 1');
    $assert->pageTextNotContains('The number 2');
    $assert->pageTextNotContains('The number 3');

    // Check that the default letter 'x' and number '0' variants are displayed.
    $this->drupalGet('/xp/test/element', ['query' => ['options' => 'default']]);
    $assert->waitForElementVisible('css', '[data-xp-variant="x"]');
    $assert->elementExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextContains('The letter X');
    $assert->pageTextNotContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    $assert->pageTextContains('The number 0');
    $assert->pageTextNotContains('The number 1');
    $assert->pageTextNotContains('The number 2');
    $assert->pageTextNotContains('The number 3');

    // Check that letter variant 'a' is displayed.
    $this->drupalGet('/xp/test/element', ['query' => ['_variant' => 'a']]);
    $assert->waitForElementVisible('css', '[data-xp-variant="a"]');
    $assert->elementNotExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    // Check that the letter 'b' variant is displayed.
    $this->drupalGet('/xp/test/element', ['query' => ['_variant' => 'b']]);
    $assert->waitForElementVisible('css', '[data-xp-variant="b"]');
    $assert->elementNotExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextNotContains('The letter A');
    $assert->pageTextContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    // Check that the letter 'c' variant is displayed.
    $this->drupalGet('/xp/test/element', ['query' => ['_variant' => 'c']]);
    $assert->waitForElementVisible('css', '[data-xp-variant="c"]');
    $assert->elementNotExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextNotContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextContains('The letter C');

    // Check that the letter 'x' variant is displayed.
    $this->drupalGet('/xp/test/element', ['query' => ['_variant' => 'x']]);
    $assert->waitForElementVisible('css', '[data-xp-variant="x"]');
    $assert->elementNotExists('css', '[data-xp="test"][data-xp-default]');

    $assert->pageTextContains('The letter X');
    $assert->pageTextNotContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    // Check that the letter 'a' and number '1' variants are displayed.
    // @see \Drupal\xp\Controller\XpTestController
    $this->drupalGet('/xp/test/element', ['query' => ['_variant' => ['a', '1']]]);
    $assert->waitForElementVisible('css', '[data-xp-variant="a"]');
    $assert->waitForElementVisible('css', '[data-xp-variant="1"]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    $assert->pageTextNotContains('The number 0');
    $assert->pageTextContains('The number 1');
    $assert->pageTextNotContains('The number 2');
    $assert->pageTextNotContains('The number 3');

    // Check that the letter 'a' and number '1' variants are displayed
    // using Ajax.
    // @see \Drupal\xp\Controller\XpTestController
    $options = ['query' => ['options' => 'ajax', '_variant' => ['a', '1']]];
    $this->drupalGet('/xp/test/element', $options);
    $assert->waitForElementVisible('css', '[data-xp-variant="a"]');
    $assert->waitForElementVisible('css', '[data-xp-variant="1"]');

    $assert->pageTextNotContains('The letter X');
    $assert->pageTextContains('The letter A');
    $assert->pageTextNotContains('The letter B');
    $assert->pageTextNotContains('The letter C');

    $assert->pageTextNotContains('The number 0');
    $assert->pageTextContains('The number 1');
    $assert->pageTextNotContains('The number 2');
    $assert->pageTextNotContains('The number 3');

  }

}
