<?php

namespace Drupal\Tests\xp\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\BrowserTestBase;

/**
 * Defines an abstract test base for xp tests.
 */
abstract class XpBrowserTestBase extends BrowserTestBase {

  /**
   * Set default theme to stable.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp'];

  /**
   * Place breadcrumb page, tasks, and actions.
   */
  protected function placeBlocks() {
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Assert block content and paragraph types are installed.
   *
   * @param string $block_content_type
   *   Block content type.
   * @param string $paragraph_type
   *   Paragraph type.
   */
  protected function assertInstallBlockConfig($block_content_type, $paragraph_type) {
    $assert = $this->assertSession();
    $this->drupalGet('/admin/structure/block/block-content/manage/' . $block_content_type);
    $assert->statusCodeEquals(200);
    $this->drupalGet('/admin/structure/paragraphs_type/' . $paragraph_type);
    $assert->statusCodeEquals(200);
  }

  /**
   * Assert block content and paragraph types are uninstalled.
   *
   * @param string $block_content_type
   *   Block content type.
   * @param string $paragraph_type
   *   Paragraph type.
   */
  protected function assertUninstallBlockConfig($block_content_type, $paragraph_type) {
    $assert = $this->assertSession();
    $this->drupalGet('/admin/structure/block/block-content/manage/' . $block_content_type);
    $assert->statusCodeEquals(404);
    $this->drupalGet('/admin/structure/paragraphs_type/' . $paragraph_type);
    $assert->statusCodeEquals(404);
  }

  /**
   * Asserts that a select option in the current page is checked.
   *
   * @param string $id
   *   ID of select field to assert.
   * @param string $option
   *   Option to assert.
   * @param string $message
   *   (optional) A message to display with the assertion. Do not translate
   *   messages with t(). If left blank, a default message will be displayed.
   */
  protected function assertOptionSelected($id, $option, $message = NULL) {
    $option_field = $this->assertSession()->optionExists($id, $option);
    $message = $message ?: "Option $option for field $id is selected.";
    $this->assertTrue($option_field->hasAttribute('selected'), $message);
  }

}
