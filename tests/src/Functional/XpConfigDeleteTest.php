<?php

namespace Drupal\Tests\xp\Functional;

/**
 * Test the xp.module's configuration delete warning.
 *
 * @group xp
 */
class XpConfigDeleteTest extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block', 'field_ui'];

  /**
   * Tests the xp.module's configuration delete warning.
   */
  public function testDeleteWarning() {
    $session = $this->assertSession();

    $this->drupalLogin($this->rootUser);

    $paths = [
      // Block types.
      '/admin/structure/block/block-content/manage/xp_basic/delete' => 'custom block type',
      // Block type fields.
      '/admin/structure/block/block-content/manage/xp_basic/fields/block_content.xp_basic.field_xp_id/delete' => 'field',
      '/admin/structure/block/block-content/manage/xp_basic/fields/block_content.xp_basic.field_xp_notes/delete' => 'field',
      '/admin/structure/block/block-content/manage/xp_basic/fields/block_content.xp_basic.field_xp_variants/delete' => 'field',
      // Paragraph types.
      '/admin/structure/paragraphs_type/xp_variant_body/delete' => 'Paragraphs type',
      // Paragraph types fields.
      '/admin/structure/paragraphs_type/xp_variant_body/fields/paragraph.xp_variant_body.field_xp_variant_id/delete' => 'field',
      '/admin/structure/paragraphs_type/xp_variant_body/fields/paragraph.xp_variant_body.field_xp_variant_notes/delete' => 'field',
      '/admin/structure/paragraphs_type/xp_variant_body/fields/paragraph.xp_variant_body.field_xp_variant_body/delete' => 'field',
    ];
    foreach ($paths as $path => $entity_type_name) {
      $this->drupalGet($path);
      $session->responseContains("Deleting this $entity_type_name can cause unexpected errors and issues.");
      $session->fieldExists('confirm');
    }
  }

}
