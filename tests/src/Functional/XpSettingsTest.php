<?php

namespace Drupal\Tests\xp\Functional;

/**
 * Test the xp.module's settings.
 *
 * @group xp
 */
class XpSettingsTest extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp', 'xp_test_element', 'xp_example_integration'];

  /**
   * Tests the xp.module's settings.
   */
  public function testSettings() {
    $session = $this->assertSession();

    // Check that xp_example_integration.module js is being included.
    // @see xp_example_integration_page_attachments()
    // @see xp_example_integration_element_info_alter()
    $this->drupalGet('/xp/test/element');
    $session->responseContains('xp_example_integration.page.js');
    $session->responseContains('xp_example_integration.element.js');

    // Disable External Personalization (XP) integration.
    $admin_user = $this->createUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalPostForm('/admin/config/content/xp', ['status' => FALSE], 'Save configuration');
    $this->drupalLogout();

    // Check that xp_example_integration.module js is being included.
    $this->drupalGet('/xp/test/element');
    $session->responseNotContains('xp_example_integration.page.js');
    $session->responseNotContains('xp_example_integration.element.js');
  }

}
