<?php

namespace Drupal\xp_test_element\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\xp\XpAjaxResponseBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for external personalization element test routes.
 */
class XpTestElementController extends ControllerBase {

  /**
   * The AJAX response builder.
   *
   * @var \Drupal\xp\XpAjaxResponseBuilderInterface
   */
  protected $ajaxResponseBuilder;

  /**
   * The XpBlockVariantController constructor.
   *
   * @param \Drupal\xp\XpAjaxResponseBuilderInterface $ajax
   *   The AJAX response builder.
   */
  public function __construct(XpAjaxResponseBuilderInterface $ajax) {
    $this->ajaxResponseBuilder = $ajax;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('xp.ajax_response_builder')
    );
  }

  /**
   * Get test elements data.
   *
   * @return array
   *   An associative array containing test elements data.
   *   (i.e. letters and numbers)
   */
  protected function getElementsData() {
    $data = [];
    $data['letters'] = [
      'title' => $this->t('Letters'),
      'variants' => [
        'x' => $this->t('The letter X'),
        'a' => $this->t('The letter A'),
        'b' => $this->t('The letter B'),
        'c' => $this->t('The letter C'),
      ],
    ];
    $data['numbers'] = [
      'title' => $this->t('Numbers'),
      'variants' => [
        '0' => $this->t('The number 0'),
        '1' => $this->t('The number 1'),
        '2' => $this->t('The number 2'),
        '3' => $this->t('The number 3'),
      ],
    ];
    return $data;
  }

  /**
   * Returns external personalization test element response.
   */
  public function index(Request $request) {
    $options = (array) ($request->query->get('options') ?: []);
    $options = $options ? array_combine($options, $options) : [];

    $build = [];
    // Options.
    $build['options'] = $this->buildTestOptions();
    $build['#cache']['contexts'] = ['url.query_args:options'];
    // Elements.
    $elements_data = $this->getElementsData();
    foreach ($elements_data as $element_name => $element_data) {
      $build[$element_name] = $this->buildTestElement($element_name, $element_data, $options);
    }
    return $build;
  }

  /**
   * Returns external personalization test element variant response.
   */
  public function variant(Request $request, $name) {
    $item = $request->query->all();

    $data = $this->getElementsData();
    $content = NestedArray::getValue(
      $data,
      [$name, 'variants', $item['variant']]
    );

    $build = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'border: 5px solid #ccc; padding: 10px; text-align: center; font-size: 5em;',
      ],
      'content' => ['#markup' => $content],
    ];

    return $this->ajaxResponseBuilder->buildResponse($item, $build);
  }

  /**
   * Build test options links.
   *
   * @return array
   *   A render array containing test options links..
   */
  protected function buildTestOptions() {
    $links = [
      'default' => [
        'url' => Url::fromRoute(
          'xp_test_element',
          [],
          ['query' => ['options' => 'default']],
        ),
        'title' => $this->t('default'),
      ],
      'ajax' => [
        'url' => Url::fromRoute(
          'xp_test_element',
          [],
          ['query' => ['options' => 'ajax']],
        ),
        'title' => $this->t('ajax'),
      ],
      'default_ajax' => [
        'url' => Url::fromRoute(
          'xp_test_element',
          [],
          ['query' => ['options' => ['default', 'ajax']]]),
        'title' => $this->t('default+ajax'),
      ],
    ];
    return [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
      'options' => [
        '#type' => 'item',
        '#title' => $this->t('Options:'),
        'links' => [
          '#theme' => 'links',
          '#links' => $links,
          '#attributes' => ['class' => ['inline']],
        ],
      ],
    ];
  }

  /**
   * Build XP example element.
   *
   * @param string $name
   *   The example element's name.
   * @param array $data
   *   Associative array of element data containing title and variants.
   * @param array $options
   *   Options to be passed to the XP element.
   *
   * @return array
   *   A render array containing the XP example element with test links.
   */
  protected function buildTestElement($name, array $data, array $options) {
    $type = 'test';
    $context = $name . '-context';
    $id = $name . '-id';

    $variants = [];
    $links = [];
    foreach ($data['variants'] as $variant_id => $variant_content) {
      $variants[$variant_id] = [
        '#type' => 'container',
        '#attributes' => [
          'style' => 'border: 5px solid #ccc; padding: 10px; text-align: center; font-size: 5em;',
        ],
        'content' => ['#markup' => $variant_content],
      ];
      $item = [
        'type' => $type,
        'id' => $id,
        'variant' => $variant_id,
      ];
      $links[$variant_id] = [
        'url' => Url::fromRoute('xp_test_element', [], ['query' => ['_variant' => $variant_id]]),
        'title' => $variant_id,
        'attributes' => [
          'data-xp-trigger' => Json::encode($item),
        ],
      ];
    }

    $build = [];
    $build['title'] = [
      '#markup' => $data['title'],
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    // Links with trigger library.
    $build['links'] = [
      '#theme' => 'links',
      '#links' => $links,
      '#attributes' => ['class' => ['inline']],
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    $build['#attached']['library'][] = 'xp/xp.trigger';
    // Element.
    $build['element'] = [
      '#type' => 'xp',
      '#xp_type' => $type,
      '#xp_context' => $context,
      '#xp_id' => $id,
      '#xp_variants' => $variants,
      '#xp_options' => $options,
    ];
    // If Ajax is enabled, add route name and parameters the element.
    if (!empty($options['ajax'])) {
      $build['element'] += [
        '#xp_ajax' => [
          'route_name' => 'xp_test_element,variant',
          'route_parameters' => ['name' => $name],
        ],
      ];
    }
    return $build;
  }

}
