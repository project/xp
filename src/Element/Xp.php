<?php

namespace Drupal\xp\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * Provides a render element to display an external personalization container.
 *
 * @RenderElement("xp")
 */
class Xp extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderXp'],
      ],
      '#xp_type' => NULL,
      '#xp_context' => NULL,
      '#xp_id' => NULL,
      '#xp_variants' => [],
      '#xp_options' => [
        'default' => FALSE,
      ],
      '#xp_ajax' => [
        'route_name' => NULL,
        'route_parameters' => [],
      ],
    ];
  }

  /**
   * External personalization element pre render callback.
   */
  public static function preRenderXp($element) {
    $element['#xp_options'] += [
      'default' => FALSE,
    ];
    $element['#xp_ajax'] += [
      'route_name' => NULL,
      'route_parameters' => [],
    ];

    // Output variants.
    $element['variants'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['xp'],
        'data-xp' => $element['#xp_type'],
        'data-xp-context' => $element['#xp_context'],
        'data-xp-id' => $element['#xp_id'],
        'data-xp-default' => !empty($element['#xp_options']['default']) ? TRUE : NULL,
      ],
    ];

    // Use Ajax for lazy loading.
    $use_ajax = $element['#xp_ajax']['route_name'] && $element['#xp_ajax']['route_parameters'];
    // Always render the default (1st) variant.
    $is_default = !empty($element['#xp_options']['default']);
    foreach ($element['#xp_variants'] as $variant_id => $variant) {
      $attributes = [
        'class' => ['xp-variant'],
        'data-xp-variant' => $variant_id,
      ];
      if (!$is_default && $use_ajax) {
        $route_name = $element['#xp_ajax']['route_name'];
        $route_parameters = $element['#xp_ajax']['route_parameters'];
        $element['variants'][$variant_id] = [
          '#type' => 'container',
          '#attributes' => $attributes + [
            'data-xp-variant-url' => Url::fromRoute($route_name, $route_parameters)->setAbsolute()->toString(),
          ],
        ];
      }
      else {
        $element['variants'][$variant_id] = [
          '#type' => 'container',
          '#attributes' => $attributes,
          'content' => is_array($variant) ? $variant : ['#markup' => $variant],
        ];
      }
      $is_default = FALSE;
    }

    // Attach XP css.
    $element['#attached']['library'][] = 'xp/xp.css';

    // Attach Drupal's Ajax library if it needed.
    if ($use_ajax) {
      $element['#attached']['library'][] = 'core/drupal.ajax';
    }

    // Attach XP test javascript which user ?_variant=..
    if (\Drupal::currentUser()->hasPermission('test xp')) {
      $element['#attached']['library'][] = 'xp/xp.test';
      $element['#cache']['contexts'] = ['url.query_args:_variant'];
    }

    // Add xp.settings as cache dependency to ensure everything is reload when
    // External Personalization (XP) integration is enabled/disabled.
    // @see \Drupal\xp\Form\XpSettingsForm
    $renderer = \Drupal::service('renderer');
    $config = \Drupal::config('xp.settings');
    $renderer->addCacheableDependency($element, $config);

    return $element;
  }

}
