<?php

namespace Drupal\xp;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * External personalization setup service.
 */
class XpSetup implements XpSetupInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Demo ID used for tracking created entities.
   *
   * @var string|null
   */
  protected $id;

  /**
   * Constructs a XpSetup object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, StateInterface $state) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function setId($id) {
    $this->id = $id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($entity_type_id, array $values) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);

    // Set default langcode for content entities.
    if ($storage instanceof ContentEntityStorageInterface) {
      $values['langcode'] = $this->languageManager->getDefaultLanguage()->getId();
    }

    $entity = $storage->create($values);
    $entity->save();
    $this->trackEntity($entity);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function createFile($source_path, $destination_uri) {
    $this->fileSystem->copy($source_path, $this->fileSystem->realpath($destination_uri));
    return $this->createEntity('file', ['uri' => $destination_uri]);
  }

  /**
   * Track a created entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   */
  protected function trackEntity(EntityInterface $entity) {
    // Track entities that are create when this module is installed.
    $id = $this->getId();
    if (empty($id)) {
      return;
    }

    $demo_id = $this->getId() . '_entities';
    $entities = $this->state->get($demo_id, []);
    $entities[] = $entity->getEntityTypeId() . ':' . $entity->id();
    $this->state->set($demo_id, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEntities() {
    $id = $this->getId();
    if (empty($id)) {
      return;
    }

    $demo_id = $this->getId() . '_entities';
    $entities = $this->state->get($demo_id, []);
    foreach ($entities as $entity_info) {
      [$entity_type_id, $entity_id] = explode(':', $entity_info);
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      if ($storage) {
        $entity = $storage->load($entity_id);
        if ($entity) {
          $entity->delete();
        }
      }
    }
    $this->state->delete($demo_id);
  }

}
