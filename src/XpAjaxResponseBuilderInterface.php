<?php

namespace Drupal\xp;

/**
 * Provides an interface for building XP AJAX  responses.
 */
interface XpAjaxResponseBuilderInterface {

  /**
   * Create an Ajax response that display the selected variant.
   *
   * @param array $item
   *   An XP item with type, id, and variant.
   * @param array|string|\Drupal\Component\Render\MarkupInterface $build
   *   Render array or markup to be returned.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An Ajax response with commands that display the selected variant.
   */
  public function buildResponse(array $item, $build);

}
