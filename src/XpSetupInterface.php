<?php

namespace Drupal\xp;

/**
 * Defines an interface for XP setup classes.
 */
interface XpSetupInterface {

  /**
   * Set the demo id.
   *
   * @param string $id
   *   The demo id.
   *
   * @return $this
   */
  public function setId($id);

  /**
   * Get the demo id.
   *
   * @return string
   *   The demo id.
   */
  public function getId();

  /**
   * Create and track an entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $values
   *   The entity's initial values.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A saved entity.
   */
  public function createEntity($entity_type_id, array $values);

  /**
   * Create file.
   *
   * @param string $source_path
   *   File source path.
   * @param string $destination_uri
   *   File destination Uri.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Created and saved file.
   */
  public function createFile($source_path, $destination_uri);

  /**
   * Delete tracked entities when the demo is uninstalled.
   */
  public function deleteEntities();

}
