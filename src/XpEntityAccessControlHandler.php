<?php

namespace Drupal\xp;

use Drupal\Core\Access\AccessResult;
use Drupal\xp\Utility\XpHelper;

/**
 * External personalization entity access checker.
 */
class XpEntityAccessControlHandler {

  /**
   * Check external personalization entity access.
   */
  public static function checkAccess($entity_type, $entity_id) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
    return AccessResult::allowedIf(XpHelper::isXpEntity($entity) && $entity->access('view'));
  }

}
