<?php

namespace Drupal\xp\Utility;

use Drupal\Core\Entity\EntityInterface;

/**
 * Helper class for external personalization.
 */
class XpHelper {

  /**
   * Determine if an entity is an external personalization entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   An entity.
   *
   * @return bool
   *   TRUE if an entity is an external personalization entity.
   */
  public static function isXpEntity(EntityInterface $entity = NULL) {
    return ($entity && (strpos($entity->id(), 'xp_') === 0 || strpos($entity->bundle(), 'xp_') === 0));
  }

  /**
   * Cleanup XP item.
   *
   * @param array $item
   *   An XP item.
   *
   * @return array
   *   An XP items with only the type, id, and variant properties.
   */
  public static function cleanupItem(array $item) {
    $defaults = [
      'type' => '',
      'id' => '',
      'variant' => '',
    ];
    return array_intersect_key($item + $defaults, $defaults);
  }

}
