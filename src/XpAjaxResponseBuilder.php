<?php

namespace Drupal\xp;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\xp\Utility\XpHelper;

/**
 * External personalization Ajax service.
 */
class XpAjaxResponseBuilder implements XpAjaxResponseBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function buildResponse(array $item, $build) {
    $item = XpHelper::cleanupItem($item);

    // Get the XP element and XP variant selectors.
    $attribute_selectors = $this->getAttributeSelectors($item);
    $variant_selector = $attribute_selectors['variant'];
    unset($attribute_selectors['variant']);
    $xp_selector = implode('', $attribute_selectors);

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand($xp_selector . ' ' . $variant_selector, $build));
    return $response;
  }

  /**
   * Get XP element attributes selectors.
   *
   * @param array $item
   *   An XP item.
   *
   * @return array
   *   An associative array XP element attributes selectors keyed by
   *   item property.
   */
  protected function getAttributeSelectors(array $item) {
    $selectors = [];
    foreach ($item as $key => $value) {
      $name = ($key === 'type') ? 'data-xp' : 'data-xp-' . $key;
      $value = str_replace('"', '\"', $value);
      $selectors[$key] = ($value === '')
        ? '[' . $name . ']'
        : '[' . $name . '="' . $value . '"]';
    }
    return $selectors;
  }

}
