<?php

namespace Drupal\xp\Controller;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\xp\XpAjaxResponseBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for External Personalization (XP) entity routes.
 */
class XpEntityController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The AJAX response builder.
   *
   * @var \Drupal\xp\XpAjaxResponseBuilderInterface
   */
  protected $ajaxResponseBuilder;

  /**
   * The XpBlockVariantController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\xp\XpAjaxResponseBuilderInterface $ajax_response_builder
   *   The XP ajax service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, XpAjaxResponseBuilderInterface $ajax_response_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->ajaxResponseBuilder = $ajax_response_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('xp.ajax_response_builder')
    );
  }

  /**
   * Return variant Ajax response.
   */
  public function variant(Request $request, $entity_type, $entity_id, $view_mode = 'full') {
    $item = $request->query->all() + ['variant' => ''];

    // Load the element.
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

    // Get the variants render array.
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
    $build = $view_builder->view($entity, $view_mode);
    $variant = NestedArray::getValue(
      $build,
      ['xp', '#xp_variants', $item['variant']]
    );

    // Throw page not found if the variant does not exist.
    if (empty($variant)) {
      throw new NotFoundHttpException();
    }

    return $this->ajaxResponseBuilder->buildResponse($item, $variant);
  }

}
