<?php

namespace Drupal\xp\Controller;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for external personalization test routes.
 */
class XpTestController extends ControllerBase {

  /**
   * Returns external personalization block test response.
   */
  public function index(Request $request) {
    $data = $request->query->get('data') ?: [];
    $location = $request->query->get('location') ?: '';
    $url = UrlHelper::parse($location);
    $variant = NestedArray::getValue($url, ['query', '_variant']);
    // Cast variant to any array so that we can support multiple variants.
    $variant = (array) $variant;
    foreach ($data as &$xp) {
      // Set defaults.
      $xp += [
        'type' => 'custom',
        'id' => NULL,
        'variant' => NULL,
      ];
      switch ($xp['type']) {
        case 'block':
          $xp['variant'] = $this->getBlockContentActiveVariant($xp['id'], (array) $variant);
          break;

        case 'custom':
        default:
          $xp['variant'] = $variant;
          break;
      }
    }

    return JsonResponse::create($data);
  }

  /**
   * Get active variant for block content.
   *
   * @param string $id
   *   Personalization ID.
   * @param array $variants
   *   Variant IDs.
   *
   * @return null|string
   *   Active variant for block content.
   */
  protected function getBlockContentActiveVariant($id, array $variants) {
    $block_content_storage = $this->entityTypeManager()->getStorage('block_content');
    $entity_ids = $block_content_storage->getQuery()
      ->condition('field_xp_id', $id)
      ->execute();
    if (empty($entity_ids)) {
      return NULL;
    }

    /** @var \Drupal\block_content\BlockContentInterface[] $block_contents */
    $block_contents = $block_content_storage->loadMultiple($entity_ids);
    foreach ($block_contents as $block_content) {
      if ($block_content->hasField('field_xp_variants')) {
        foreach ($block_content->field_xp_variants as $item) {
          $variant_id = $item->entity->field_xp_variant_id->value;
          if (in_array($variant_id, $variants)) {
            return $variant_id;
          }
        }
      }
    }

    return NULL;
  }

}
