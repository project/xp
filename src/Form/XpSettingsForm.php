<?php

namespace Drupal\xp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure External Personalization (XP) settings for this site.
 */
class XpSettingsForm extends ConfigFormBase {

  /**
   * A cache backend interface instance.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * A element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $elementInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cacheRender = $container->get('cache.render');
    $instance->elementInfo = $container->get('element_info');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xp_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['xp.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];
    $form['general']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable External Personalization (XP) integration'),
      '#description' => $this->t('If unchecked, all External Personalization (XP) integrations will be disabled but default variants will still be displayed.'),
      '#return_value' => TRUE,
      '#default_value' => $this->config('xp.settings')->get('status'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('xp.settings')
      ->set('status', (boolean) $form_state->getValue('status'))
      ->save();

    // Clear element info and render cache to allow integrations to be disabled.
    // @see xp_example_integration_element_info_alter()
    $this->elementInfo->clearCachedDefinitions();
    $this->cacheRender->invalidateAll();

    parent::submitForm($form, $form_state);
  }

}
