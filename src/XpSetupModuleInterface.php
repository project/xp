<?php

namespace Drupal\xp;

/**
 * Defines an interface for XP setup module classes.
 */
interface XpSetupModuleInterface {

  /**
   * Install the module.
   */
  public function install();

  /**
   * Uninstall the module.
   */
  public function uninstall();

}
