<?php

namespace Drupal\Tests\xp_block_paragraphs\Functional;

use Drupal\Tests\xp\Functional\XpBrowserTestBase;

/**
 * Test the xp_block_paragraphs.module's installation and uninstallation.
 *
 * @group xp
 */
class XpBlockParagraphsInstallTest extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block_paragraphs'];

  /**
   * Tests the xp_block_paragraphs.module's installation and uninstallation.
   */
  public function testInstallUninstall() {
    $this->drupalLogin($this->rootUser);
    $this->assertInstallBlockConfig('xp_paragraph', 'xp_variant_paragraph');
    \Drupal::service('module_installer')->uninstall(static::$modules);
    $this->assertUninstallBlockConfig('xp_paragraph', 'xp_variant_paragraph');
  }

}
