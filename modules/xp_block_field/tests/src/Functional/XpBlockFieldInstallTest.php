<?php

namespace Drupal\Tests\xp_block_field\Functional;

use Drupal\Tests\xp\Functional\XpBrowserTestBase;

/**
 * Test the xp_block_field.module's installation and uninstallation.
 *
 * @group xp
 */
class XpBlockFieldInstallTest extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block_field'];

  /**
   * Tests the xp_block_field.module's installation and uninstallation.
   */
  public function testInstallUninstall() {
    $this->drupalLogin($this->rootUser);
    $this->assertInstallBlockConfig('xp_block', 'xp_variant_block');
    \Drupal::service('module_installer')->uninstall(static::$modules);
    $this->assertUninstallBlockConfig('xp_block', 'xp_variant_block');
  }

}
