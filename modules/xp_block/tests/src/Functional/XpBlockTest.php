<?php

namespace Drupal\Tests\xp_block\Functional;

/**
 * Test the xp_block.module's UI enhancements and breadcrumbs.
 *
 * @group xp
 */
class XpBlockTest extends XpBlockTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block'];

  /**
   * Tests the xp_block.module's UI enhancements and breadcrumbs.
   */
  public function testXpBlock() {
    $assert = $this->assertSession();

    $this->placeBlocks();

    $block_admin = $this->createUser(['administer blocks']);
    $xp_block_admin = $this->createUser(['administer xp blocks']);

    /* ********************************************************************** */

    // Check that access is denied to the XP block library.
    $this->drupalGet('/admin/structure/block/xp-block');
    $assert->statusCodeEquals(403);
    $this->drupalLogin($xp_block_admin);

    // Get the XP block library page.
    $this->drupalGet('/admin/structure/block/xp-block');

    // Check that access is allowed to the XP block library.
    $assert->statusCodeEquals(200);

    // Check the XP block list empty message and link.
    // @see \Drupal\xp_block\Plugin\views\area\XpBlockListingEmpty
    $assert->responseContains('There are no XP blocks available.');
    $assert->linkExistsExact('XP block');
    $assert->linkByHrefExists('/admin/structure/block/xp-block/add', 0);

    // Check that the 'Add XP block' link existing.
    $assert->linkExistsExact('Add XP block');
    $assert->linkByHrefExists('/admin/structure/block/xp-block/add', 1);

    // Create basic block type.
    $this->createBasicBlockType();

    // Create XP basic block.
    $block_content = $this->createXpBasicBlock();

    // Confirm that XP block label visibility is hidden.
    // @see xp_block_form_block_form_alter()
    // @see xp_block_preprocess_block()
    $this->drupalGet('/admin/structure/block/manage/xp_basic_block');
    $assert->elementNotExists('css', '#edit-settings-label-display');

    // Check that XP block's info label is changed to
    // 'Personalization description'.
    // @see xp_block_form_block_content_form_alter.
    $this->drupalGet('/block/' . $block_content->id());
    $assert->responseContains('<label for="edit-info-0-value" class="js-form-required form-required">Personalization description</label>');

    // Get the XP block library page.
    $this->drupalGet('/admin/structure/block/xp-block');

    // Check the XP block list empty message and link DO NOT appear.
    $assert->responseNotContains('There are no XP blocks available.');
    $assert->linkNotExistsExact('XP block');

    // Check that XP basic block exists.
    // @see \Drupal\xp_block\Controller\XpBlockContentController
    $assert->linkExistsExact('Basic Example');
    $assert->linkByHrefExists('/block/' . $block_content->id());

    // Check that XP block library has no ?destination query strings.
    // @see xp_block_entity_operation_alter().
    $assert->responseNotContains('?destination=');

    // Check that 'Add custom block' page only shows custom blocks.
    // @see \Drupal\xp_block\Controller\XpBlockContentController
    $this->drupalLogin($block_admin);
    $this->drupalGet('/block/add');
    $assert->responseContains('<h1>Add custom block</h1>');
    $assert->elementExists('css', '#block-content-basic-block-form');

    // Check that 'Add XP block' page only shows XP blocks and breadcrumb.
    $this->drupalLogin($xp_block_admin);
    $this->drupalGet('/admin/structure/block/xp-block/add');
    $assert->responseContains('<h1>Add XP block</h1>');
    $assert->elementExists('css', '#block-content-xp-basic-form');
    $this->assertBreadcrumb();

    // Check Add XP basic custom block' page breadcrumb.
    $this->drupalGet('/block/add/xp_basic');
    $assert->linkExistsExact('XP block library');
    $assert->linkByHrefExists('/admin/structure/block/xp-block');

    // Check XP breadcrumb.
    $this->drupalGet('/block/' . $block_content->id());
    $this->assertBreadcrumb();
    $this->drupalGet('/block/' . $block_content->id() . '/xp');
    $this->assertBreadcrumb();
    $this->drupalGet('/block/' . $block_content->id() . '/delete');
    $this->assertBreadcrumb();

    // Check that ?destination is passed between XP block tabs.
    // @see xp_block_menu_local_tasks_alter();
    $options = ['query' => ['destination' => '/test']];
    $this->drupalGet('/block/' . $block_content->id(), $options);
    $assert->linkByHrefExists('/block/' . $block_content->id() . '?destination=/test');
    $assert->linkByHrefExists('/block/' . $block_content->id() . '/xp?destination=/test');
    $assert->linkByHrefExists('/block/' . $block_content->id() . '/delete?destination=/test');

    $this->drupalGet('/block/' . $block_content->id() . '/xp', $options);
    $assert->linkByHrefExists('/block/' . $block_content->id() . '?destination=/test');
    $assert->linkByHrefExists('/block/' . $block_content->id() . '/xp?destination=/test');
    $assert->linkByHrefExists('/block/' . $block_content->id() . '/delete?destination=/test');

    // Check that XP block saving redirects to the XP block library page.
    // @eee xp_block_form_block_content_form_alter().
    $this->drupalPostForm('/block/' . $block_content->id(), [], 'Save');
    $assert->addressEquals('/admin/structure/block/xp-block');

    // Check that XP block delete form has update title..
    // @eee  xp_block_form_alter().
    $this->drupalGet('/block/' . $block_content->id() . '/delete');
    $assert->responseContains('<h1>Are you sure you want to delete the XP block <em class="placeholder">Basic Example</em>?</h1>');
    $assert->linkNotExists('/admin/structure/block/block-content');
    $assert->linkByHrefExists('/admin/structure/block/xp-block');

    // Check that XP block deleting redirects to the XP block library page.
    // @eee xp_block_form_block_content_form_alter().
    $this->drupalPostForm('/block/' . $block_content->id() . '/delete', [], 'Delete');
    $assert->addressEquals('/admin/structure/block/xp-block');
  }

  /**
   * Assert XP block library breadcrumb exists.
   *
   * @see \Drupal\xp_block\Breadcrumb\XpBlockBreadcrumbBuilder
   */
  protected function assertBreadcrumb() {
    $assert = $this->assertSession();
    $assert->linkExistsExact('XP block library');
    $assert->linkByHrefExists('/admin/structure/block/xp-block');
  }

}
