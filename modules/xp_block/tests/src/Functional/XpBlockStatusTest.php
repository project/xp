<?php

namespace Drupal\Tests\xp_block\Functional;

/**
 * Test the xp_block.module's status (publish/unpublish) support.
 *
 * @group xp
 */
class XpBlockStatusTest extends XpBlockTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block'];

  /**
   * Test the xp_block.module's status (publish/unpublish) support.
   */
  public function testXpBlockStatus() {
    $assert = $this->assertSession();

    $this->createBasicBlockType();

    // Create XP basic block.
    $block_content = $this->createXpBasicBlock();

    // Check that all variants are visible.
    $this->drupalGet('<front>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="x">X</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="a">A</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="b">B</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="c">C</div>');

    // Unpublish A variant.
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $block_content->field_xp_variants->get(1)->entity;
    $paragraph->status->value = FALSE;
    $paragraph->save();

    // Check that variant A is NOT visible.
    $this->drupalGet('<front>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="x">X</div>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="a">A</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="b">B</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="c">C</div>');

    // Unpublish the block.
    $block_content->status->value = FALSE;
    $block_content->save();

    // Check that variant A is NOT visible.
    $this->drupalGet('<front>');
    $assert->responseNotContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="x">X</div>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="a">A</div>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="b">B</div>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="c">C</div>');

    // Create and login block administrator.
    $admin_user = $this->createUser(['administer xp blocks']);
    $this->drupalLogin($admin_user);

    // Check that block administrator can see the block but not the
    // unpublished variant.
    $this->drupalGet('<front>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="x">X</div>');
    $assert->responseNotContains('<div class="xp-variant" data-xp-variant="a">A</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="b">B</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="c">C</div>');

    // Create and login block administrator with view unpublished
    // paragraphs permission.
    $permissions = ['administer xp blocks', 'view unpublished paragraphs'];
    $admin_user = $this->createUser($permissions);
    $this->drupalLogin($admin_user);

    // Check that block administrator with permission can see the
    // block and variants.
    $this->drupalGet('<front>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="x">X</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="a">A</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="b">B</div>');
    $assert->responseContains('<div class="xp-variant" data-xp-variant="c">C</div>');
  }

}
