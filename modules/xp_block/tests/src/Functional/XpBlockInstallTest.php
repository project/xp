<?php

namespace Drupal\Tests\xp_block\Functional;

use Drupal\Tests\xp\Functional\XpBrowserTestBase;

/**
 * Test the xp_block.module's installation and uninstallation.
 *
 * @group xp
 */
class XpBlockInstallTest extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block'];

  /**
   * Tests the xp_block.module's installation and uninstallation.
   */
  public function testInstallUninstall() {
    $this->drupalLogin($this->rootUser);
    $this->assertInstallBlockConfig('xp_basic', 'xp_variant_body');
    \Drupal::service('module_installer')->uninstall(static::$modules);
    $this->assertUninstallBlockConfig('xp_basic', 'xp_variant_body');
  }

}
