<?php

namespace Drupal\Tests\xp_block\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\filter\Entity\FilterFormat;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\xp\Functional\XpBrowserTestBase;

/**
 * Test the xp_block.module's UI enhancements and breadcrumbs.
 *
 * @group xp
 */
abstract class XpBlockTestBase extends XpBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_block'];

  /**
   * Create basic block content type.
   */
  protected function createBasicBlockType() {
    $block_content_type = BlockContentType::create([
      'id' => 'Basic block',
      'label' => 'basic',
      'revision' => FALSE,
    ]);
    $block_content_type->save();
    block_content_add_body_field($block_content_type->id());
  }

  /**
   * Create an XP basic block.
   *
   * @return \Drupal\block_content\BlockContentInterface
   *   An XP basic block instance.
   */
  protected function createXpBasicBlock() {
    $basic_html_format = FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<p> <br> <strong> <a> <em>',
          ],
        ],
      ],
    ]);
    $basic_html_format->save();

    $variants = [
      'x' => 'X',
      'a' => 'A',
      'b' => 'B',
      'c' => 'C',
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $values[] = Paragraph::create([
        'type' => 'xp_variant_body',
        'field_xp_variant_id' => ['value' => $id],
        'field_xp_variant_body' => [
          'value' => $text,
          'format' => 'basic_html',
        ],
      ]);
    }
    $block_content = BlockContent::create([
      'info' => 'Basic Example',
      'type' => 'xp_basic',
      'field_xp_id' => ['value' => 'xp_basic_example'],
      'field_xp_notes' => ['value' => 'This is an example of a basic personalized HTML block.'],
      'field_xp_variants' => $values,
    ]);
    $block_content->save();
    $block = $this->drupalPlaceBlock('block_content:' . $block_content->uuid(), ['id' => 'xp_basic_block']);
    $block->save();
    return $block_content;
  }

  /**
   * Create variants body.
   *
   * @return array
   *   Variants HTML field values.
   */
  protected function getFieldVariantsBody() {
    $variants = [
      'x' => 'X',
      'a' => 'A',
      'b' => 'B',
      'c' => 'C',
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $values[] = Paragraph::create([
        'type' => 'xp_variant_body',
        'field_xp_variant_id' => ['value' => $id],
        'field_xp_variant_notes' => ['value' => 'The letter ' . $text],
        'field_xp_variant_body' => [
          'value' => '<h3>' . $text . '</h3>',
          'format' => 'basic_html',
        ],
      ]);
    }
    return $values;
  }

}
