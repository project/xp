<?php

namespace Drupal\xp_block\Controller;

use Drupal\block_content\BlockContentTypeInterface;
use Drupal\block_content\Controller\BlockContentController;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\xp\Utility\XpHelper;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for external personalization block routes.
 */
class XpBlockContentController extends BlockContentController {

  /**
   * {@inheritdoc}
   */
  public function add(Request $request) {
    $types = $this->blockContentTypeStorage->loadMultiple();

    // Hide all XP blocks on 'block_content.add_page' routes (/block/add).
    // Show only XP blocks on 'xp_block.add_page' routes
    // (/admin/structure/block/xp-block/add).
    $is_xp_add_page = (\Drupal::routeMatch()->getRouteName() === 'xp_block.add_page');
    foreach ($types as $type_id => $type) {
      if (
        ($is_xp_add_page && !XpHelper::isXpEntity($type))
        ||
        (!$is_xp_add_page && XpHelper::isXpEntity($type))
      ) {
        unset($types[$type_id]);
      }
    }

    if ($types && count($types) == 1) {
      $type = reset($types);
      return $this->addForm($type, $request);
    }
    if (count($types) === 0) {
      return [
        '#markup' => $this->t('You have not created any block types yet. Go to the <a href=":url">block type creation page</a> to add a new block type.', [
          ':url' => Url::fromRoute('block_content.type_add')->toString(),
        ]),
      ];
    }

    return ['#theme' => 'block_content_add_list', '#content' => $types];
  }

  /**
   * Check XP block content add access.
   *
   * @param \Drupal\block_content\BlockContentTypeInterface $block_content_type
   *   The block content type.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function checkAccess(BlockContentTypeInterface $block_content_type, AccountInterface $account) {
    // Always check for 'administer blocks' permission.
    // This is the default access rule.
    // @see block_content.routing.yml => block_content.add_form
    if ($account->hasPermission('administer blocks')) {
      return AccessResult::allowed();
    }
    // If block content type is an XP block, check 'administer xp blocks'
    // permission.
    elseif (XpHelper::isXpEntity($block_content_type)
      && $account->hasPermission('administer xp blocks')) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::neutral();
    }
  }

}
