<?php

namespace Drupal\xp_block\Controller;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\xp_block\Utility\XpBlockHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Returns responses for External Personalization (XP) Block routes.
 *
 * @see https://stackoverflow.com/questions/27382433/how-can-i-export-a-csv-using-symfonys-streamedresponse
 */
class XpBlockDownloadController extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The XpBlockDownloadController constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $data_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(DateFormatterInterface $data_formatter, EntityTypeManagerInterface $entity_type_manager) {
    $this->dateFormatter = $data_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Build XP blocks (CSV) response.
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   *   Response containing a CSV.
   */
  public function buildBlocks() {
    $response = new StreamedResponse(function () {
      $handle = fopen('php://output', 'r+');

      // Header.
      fputcsv($handle, [
        (string) $this->t('Description'),
        (string) $this->t('Notes'),
        (string) $this->t('Type'),
        (string) $this->t('ID'),
        (string) $this->t('Options'),
        (string) $this->t('Variants'),
        (string) $this->t('Status'),
        (string) $this->t('Updated'),
      ]);

      // Rows.
      $storage = $this->entityTypeManager->getStorage('block_content');
      $entity_ids = $storage->getQuery()
        ->condition('type', 'xp_%', 'LIKE')
        ->sort('type')
        ->sort('info')
        ->execute();
      if ($entity_ids) {
        $entity_ids_chunk = array_chunk($entity_ids, 500);
        foreach ($entity_ids_chunk as $entity_ids) {
          /** @var \Drupal\block_content\BlockContentInterface[] $block_contents */
          $block_contents = $storage->loadMultiple($entity_ids);
          foreach ($block_contents as $block_content) {
            $record = [];
            $record[] = $block_content->label();
            $record[] = $block_content->field_xp_notes->value;
            $record[] = $block_content->bundle();
            $record[] = $block_content->field_xp_id->value;
            $record[] = implode(';', XpBlockHelper::getOptions($block_content));
            $record[] = implode(PHP_EOL, $this->getVariantsInformation($block_content));
            $record[] = $block_content->status->value ? $this->t('Published') : $this->t('Unpublished');
            $record[] = $this->dateFormatter->format($block_content->getChangedTime(), 'html_datetime');
            fputcsv($handle, $record);
          }
        }
      }

      fclose($handle);
    });

    $response->headers->set('Content-Type', 'application/force-download');
    $response->headers->set('Content-Disposition', 'attachment; filename="xp-blocks.csv"');
    return $response;
  }

  /**
   * Build XP block variants (CSV) response.
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   *   Response containing a CSV.
   */
  public function buildVariants(BlockContentInterface $block_content) {
    $response = new StreamedResponse(function () use ($block_content) {
      $handle = fopen('php://output', 'r+');

      // Header.
      fputcsv($handle, [
        (string) $this->t('Block description'),
        (string) $this->t('Block ID'),
        (string) $this->t('Variant ID'),
        (string) $this->t('Variant status'),
        (string) $this->t('Variant notes'),
      ]);

      // Rows.
      foreach ($block_content->field_xp_variants as $item) {
        $record = [];
        $record[] = $block_content->label();
        $record[] = $block_content->field_xp_id->value;
        $record[] = $item->entity->field_xp_variant_id->value;
        $record[] = $item->entity->status->value ? $this->t('Published') : $this->t('Unpublished');
        $record[] = $item->entity->field_xp_variant_notes->value;
        fputcsv($handle, $record);
      }

      fclose($handle);
    });

    $response->headers->set('Content-Type', 'application/force-download');
    $response->headers->set('Content-Disposition', 'attachment; filename="xp-' . $block_content->field_xp_id->value . '-variants.csv"');
    return $response;
  }

  /**
   * Get block content's variants information.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return array
   *   Block content's variants information.
   */
  protected function getVariantsInformation(BlockContentInterface $block_content) {
    $variants = XpBlockHelper::getVariants($block_content);
    $info = [];
    foreach ($variants as $variant_id => $variant) {
      $info[$variant_id] = $variant_id . (!$variant['status'] ? ' [' . $this->t('Unpublished') . ']' : '');
    }
    return $info;
  }

}
