<?php

namespace Drupal\xp_block\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class XpBlockRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Override the block content add page to ensure that XP blocks are not
    // displayed on this page.
    if ($route = $collection->get('block_content.add_page')) {
      $route->setDefault('_controller', '\Drupal\xp_block\Controller\XpBlockContentController::add');
    }
    // Override the block content add form to ensure that users with
    // 'administer xp blocks' permission can add blocks.
    if ($route = $collection->get('block_content.add_form')) {
      $route->setRequirements(['_custom_access' => '\Drupal\xp_block\Controller\XpBlockContentController::checkAccess']);
    }
  }

}
