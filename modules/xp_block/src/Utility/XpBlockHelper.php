<?php

namespace Drupal\xp_block\Utility;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\xp\Utility\XpHelper;

/**
 * Helper class for external personalization blocks.
 */
class XpBlockHelper {

  /**
   * Determine if an entity is an XP block content entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   An entity.
   *
   * @return bool
   *   TRUE if an entity is an XP block content entity.
   */
  public static function isXpBlockEntity(EntityInterface $entity = NULL) {
    return (XpHelper::isXpEntity($entity) && $entity->getEntityTypeId() === 'block_content');
  }

  /**
   * Get block content's item.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   * @param string|null $variant
   *   Optional variant id.
   *
   * @return array
   *   An associative array containing a block content's type, ID, and
   *   optional variant.
   */
  public static function getItem(BlockContentInterface $block_content, $variant = NULL) {
    $item = [
      'type' => 'block',
      'id' => static::getId($block_content),
    ];
    if (isset($variant)) {
      $item['variant'] = $variant;
    }
    return $item;
  }

  /**
   * Get block content's ID.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return string
   *   Block content's ID.
   */
  public static function getId(BlockContentInterface $block_content) {
    return $block_content->field_xp_id->value;
  }

  /**
   * Get block content's notes.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return string|null
   *   block content's notes.
   */
  public static function getNotes(BlockContentInterface $block_content) {
    return $block_content->field_xp_id->notes;
  }

  /**
   * Get block content's options.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return array
   *   Block content's options.
   */
  public static function getOptions(BlockContentInterface $block_content) {
    $options = [];
    if ($block_content->hasField('field_xp_options')) {
      foreach ($block_content->field_xp_options as $item) {
        $options[$item->value] = $item->value;
      }
    }
    return $options;
  }

  /**
   * Get block content's variants.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return array
   *   An associative array containing a block content's variant information
   *   included id, status, and notes..
   */
  public static function getVariants(BlockContentInterface $block_content) {
    $variants = [];
    foreach ($block_content->field_xp_variants as $item) {
      $variant_id = $item->entity->field_xp_variant_id->value;
      $variants[$variant_id] = [
        'id' => $variant_id,
        'status' => $item->entity->status->value,
        'notes' => $item->entity->field_xp_variant_notes->value,
      ];
    }
    return $variants;
  }

  /**
   * Get block content's variants IDs.
   *
   * @param \Drupal\block_content\BlockContentInterface $block_content
   *   Block content.
   *
   * @return array
   *   Block content's variant IDs.
   */
  public static function getVariantsIds(BlockContentInterface $block_content) {
    $values = [];
    foreach ($block_content->field_xp_variants as $item) {
      $variant_id = $item->entity->field_xp_variant_id->value;
      $values[$variant_id] = $variant_id;
    }
    return $values;
  }

  /**
   * Load external personalization block content entity by the plugin id.
   *
   * @param string $plugin_id
   *   The expected block content id needs to be block_content:{uuid}.
   *
   * @return \Drupal\block_content\BlockContentInterface|null
   *   Return external personalization block content or NULL if the block
   *   content is not personalized.
   */
  public static function loadByPluginId($plugin_id) {
    if (strpos($plugin_id, 'block_content:') !== 0) {
      return NULL;
    }
    // Load the block content using the plugin to get the uuid.
    $uuid = str_replace('block_content:', '', $plugin_id);
    /** @var \Drupal\block_content\BlockContentInterface $block_content */
    $block_content = \Drupal::service('entity.repository')
      ->loadEntityByUuid('block_content', $uuid);
    return XpHelper::isXpEntity($block_content)
      ? $block_content
      : NULL;
  }

}
