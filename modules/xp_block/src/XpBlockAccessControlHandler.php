<?php

namespace Drupal\xp_block;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\xp\Utility\XpHelper;

/**
 * External personalization block access checker.
 */
class XpBlockAccessControlHandler {

  /**
   * Check external personalization access.
   */
  public static function checkAccess(BlockContentInterface $block_content) {
    return AccessResult::allowedIf(XpHelper::isXpEntity($block_content) && $block_content->access('update'));
  }

}
