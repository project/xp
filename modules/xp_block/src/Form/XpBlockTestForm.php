<?php

namespace Drupal\xp_block\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\xp_block\Utility\XpBlockHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an External Personalization test block form.
 */
class XpBlockTestForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xp_block_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $block_contents = []) {
    $current_variant_ids = $this->getCurrentVariantIds();

    // Intro.
    $form['intro'] = [
      '#markup' => $this->t('Select variants (* = default)'),
    ];

    // Blocks.
    $form['blocks'] = ['#tree' => TRUE];
    /** @var \Drupal\block_content\BlockContentInterface[] $block_contens */
    foreach ($block_contents as $block_content_id => $block_content) {
      // Add block content as cacheable dependency.
      $this->renderer->addCacheableDependency($form, $block_content);

      // Make sure the current user can view the block.
      if (!$block_content->access('view')) {
        continue;
      }

      $block_options = XpBlockHelper::getOptions($block_content);
      $block_variant_ids = XpBlockHelper::getVariantsIds($block_content);

      $default_value = (!empty($block_options['default'])) ? reset($block_variant_ids) : NULL;

      // Append * to the default variant id.
      if (isset($default_value)) {
        $block_variant_ids[$default_value] .= ' *';
      }

      // Get current variant id from current query string _variant parameters.
      if ($block_variant_ids && $current_variant_ids) {
        $intersect_variant_ids = array_intersect_key($block_variant_ids, $current_variant_ids);
        if ($intersect_variant_ids) {
          $default_value = array_key_first($intersect_variant_ids);
        }
      }

      $form['blocks'][$block_content_id] = [
        '#type' => 'select',
        '#title' => $block_content->label(),
        '#options' => $block_variant_ids,
        '#default_value' => $default_value,
      ];
    }

    // Actions.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Display variants'),
    ];
    if ($current_variant_ids) {
      $form['actions']['reset'] = [
        '#type' => 'link',
        '#title' => $this->t('Reset'),
        '#attributes' => ['class' => ['button']],
        '#url' => Url::fromRoute('<current>'),
      ];
    }

    $form['#attached']['library'][] = 'xp_block/xp_block.test';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $blocks = $form_state->getValue('blocks');
    $variants = [];
    foreach ($blocks as $variant) {
      $variants[] = $variant;
    }
    $form_state->setRedirect('<current>', [], ['query' => ['_variant' => $variants]]);
  }

  /**
   * Get variant ids from the current request.
   *
   * @return array
   *   Variant ids from the current request.
   */
  protected function getCurrentVariantIds() {
    $variants = $this->getRequest()->query->get('_variant') ?: [];
    $variants = (array) $variants;
    return $variants ? array_combine($variants, $variants) : [];
  }

}
