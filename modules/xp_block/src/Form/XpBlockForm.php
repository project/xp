<?php

namespace Drupal\xp_block\Form;

use Drupal\block_content\BlockContentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\xp_block\Utility\XpBlockHelper;

/**
 * Provides an External Personalization block form.
 */
class XpBlockForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    // Do nothing.
  }

  /**
   * Is the current request for an off-canvas settings tray.
   *
   * @return bool
   *   TRUE if the current request is for an Ajax modal/dialog.
   */
  public function isOffCanvas() {
    $wrapper_format = $this->getRequest()
      ->get(MainContentViewSubscriber::WRAPPER_FORMAT);
    return (in_array($wrapper_format, ['drupal_dialog.off_canvas'])) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Disable the submit and delete button but allow other modules to
    // restore it.
    $form['actions']['#access'] = FALSE;

    $block_content = $this->getEntity();

    $form['details'] = [
      '#type' => 'details',
      '#title' => $this->t('Personalization details'),
    ];

    // ID.
    $form['details']['id'] = [
      '#type' => 'item',
      '#title' => $this->t('ID'),
      '#description' => XpBlockHelper::getId($block_content),
    ];

    // Notes.
    $form['details']['notes'] = [
      '#type' => 'item',
      '#title' => $this->t('Notes'),
      '#description' => $this->formatNotes($block_content->field_xp_notes->value),
    ];

    // Options.
    $options = XpBlockHelper::getOptions($block_content);
    if ($options) {
      $form['details']['options'] = [
        '#type' => 'item',
        '#title' => $this->t('Options'),
        '#description' => implode('; ', $options),
      ];
    }

    // Status.
    $form['details']['status'] = [
      '#type' => 'item',
      '#title' => $this->t('Published status'),
      '#description' => $block_content->status->value ? $this->t('Published') : $this->t('Unpublished'),
    ];

    // Variants.
    $form['variants'] = [
      '#type' => 'details',
      '#title' => $this->t('Personalization variants'),
      '#open' => TRUE,
    ];
    // Variant table.
    $destination = $this->getRequest()->query->get('destination');
    $redirect_uri = $destination
      ? 'base:' . str_replace(base_path(), '', $destination)
      : NULL;

    $header = [];
    $header['variant'] = $this->t('Variant');
    $header['status'] = $this->t('Published');
    if ($redirect_uri) {
      $header['operation'] = $this->t('Operation');
    }
    $rows = [];
    $variants = XpBlockHelper::getVariants($block_content);
    foreach ($variants as $variant_id => $variant_item) {
      $row = [];
      $default = (!empty($options['default']) && count($rows) === 0)
        ? ' (' . $this->t('default') . ')'
        : '';
      $variant = [];
      if ($redirect_uri) {
        $variant['id'] = [
          '#type' => 'link',
          '#title' => $variant_id,
          '#url' => Url::fromUri($redirect_uri, ['query' => ['_variant' => $variant_id]]),
          '#attributes' => [
            'data-xp-trigger' => Json::encode(XpBlockHelper::getItem($block_content, $variant_id)),
          ],
          '#prefix' => '<strong>',
          '#suffix' => '</strong>' . $default,
        ];
      }
      else {
        $variant['id'] = [
          '#markup' => $variant_id,
          '#prefix' => '<strong>',
          '#suffix' => '</strong>' . $default,
        ];
      }
      if (!empty($variant_item['notes'])) {
        $variant['notes'] = [
          '#prefix' => '<div class="description">',
          '#suffix' => '</div>',
          '#markup' => $this->formatNotes($variant_item['notes']),
        ];
      }
      $row['variant'] = ['data' => $variant, 'width' => '80%'];
      $row['status'] = [
        'data' => ($variant_item['status']) ? $this->t('Yes') : $this->t('No'),
        'width' => '10%',
      ];
      if ($redirect_uri) {
        $row['operations'] = [
          'data' => [
            '#type' => 'link',
            '#title' => $this->t('Test'),
            '#url' => Url::fromUri($redirect_uri, ['query' => ['_variant' => $variant_id]]),
            '#attributes' => [
              'class' => ['button'],
              'data-xp-trigger' => Json::encode(XpBlockHelper::getItem($block_content, $variant_id)),
            ],
          ],
          'width' => '10%',
        ];
      }
      $rows[] = $row;
    }
    $form['variants']['table'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Variant'),
        $this->t('Published'),
        $this->t('Operation'),
      ],
      '#rows' => $rows,
    ];
    // Edit variants link.
    $query = [];
    if ($destination = $this->getRequest()->query->get('destination')) {
      $query['destination'] = $destination;
    }
    // Actions.
    $form['variants']['actions'] = [
      '#type' => 'actions',
    ];
    $form['variants']['actions']['edit'] = [
      '#type' => 'link',
      '#title' => $this->isOffCanvas() ? $this->t('Edit') : $this->t('Edit variants'),
      '#url' => $block_content->toUrl('edit-form', ['query' => $query]),
      '#attributes' => ['class' => 'button'],
    ];
    if ($this->currentUser()->hasPermission('administer xp blocks')) {
      $form['variants']['actions']['download'] = [
        '#type' => 'link',
        '#title' => $this->isOffCanvas() ? $this->t('Download') : $this->t('Download variants'),
        '#url' => Url::fromRoute('entity.block_content.xp_download', ['block_content' => $block_content->id()]),
        '#attributes' => ['class' => 'button'],
      ];
    }

    $form['#attached']['library'][] = 'xp/xp.trigger';
    if ($this->isOffCanvas()) {
      $form['#attached']['library'][] = 'xp_block/xp_block.off-canvas';
    }
    return $form;
  }

  /**
   * Format notes.
   *
   * @param string $notes
   *   Notes.
   *
   * @return string
   *   Notes with link and breaks.
   */
  protected function formatNotes($notes) {
    $filter = (object) ['settings' => ['filter_url_length' => 255]];
    return _filter_url(nl2br(htmlentities($notes)), $filter);
  }

  /**
   * Returns external personalization block admin title.
   */
  public static function title(BlockContentInterface $block_content) {
    return $block_content->label();
  }

}
