<?php

namespace Drupal\xp_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an external personalization demo block.
 *
 * @Block(
 *   id = "xp_block_test",
 *   admin_label = @Translation("XP block test"),
 *   category = @Translation("XP Block")
 * )
 */
class XpBlockTestBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity type manager.
   *
   * @var \Drupal\core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Cache demo blocks.
   *
   * @var array
   */
  protected $blocks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'block_content_ids' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['block_content_ids'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('XP blocks'),
      '#description' => $this->t('Leave blank to include all XP blocks.'),
      '#options' => $this->getBlockContentsAsOptions(),
      '#default_value' => $this->configuration['block_content_ids'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $block_content_ids = $form_state->getValue('block_content_ids');
    $block_content_ids = array_filter($block_content_ids);
    $this->configuration['block_content_ids'] = array_combine($block_content_ids, $block_content_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $blocks = $this->getBlockContents();
    return $blocks
      ? $this->formBuilder->getForm('\Drupal\xp_block\Form\XpBlockTestForm', $blocks)
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'url.query_args';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $blocks = $this->getBlockContents();
    foreach ($blocks as $block) {
      $dependencies[$block->getConfigDependencyKey()][] = $block->getConfigDependencyName();
    }
    return $dependencies;
  }

  /**
   * Get external personalization demo blocks.
   *
   * @return \Drupal\block_content\BlockContentInterface[]
   *   External personalization demo blocks.
   */
  protected function getBlockContents() {
    if (!isset($this->blocks)) {
      $block_content_storage = $this->entityTypeManager->getStorage('block_content');
      if ($this->configuration['block_content_ids']) {
        $block_content_ids = $this->configuration['block_content_ids'];
      }
      else {
        $block_content_ids = $block_content_storage
          ->getQuery()
          ->condition('type', 'xp_%', 'LIKE')
          ->sort('info')
          ->execute();
      }
      $this->blocks = $block_content_ids ? $block_content_storage->loadMultiple($block_content_ids) : [];
    }
    return $this->blocks;
  }

  /**
   * Get block content entities as options.
   *
   * @return array
   *   Associative array of block content entities.
   */
  protected function getBlockContentsAsOptions() {
    $block_content_storage = $this->entityTypeManager->getStorage('block_content');
    $block_content_ids = $block_content_storage
      ->getQuery()
      ->condition('type', 'xp_%', 'LIKE')
      ->sort('info')
      ->execute();
    $block_contents = $block_content_storage->loadMultiple($block_content_ids);
    $options = [];
    foreach ($block_contents as $block_content) {
      $options[$block_content->id()] = $block_content->label();
    }
    return $options;
  }

}
