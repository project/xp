<?php

namespace Drupal\xp_block\Plugin\views\area;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an area plugin to display a XP block add link.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("xp_block_listing_empty")
 *
 * @see \Drupal\block_content\Plugin\views\area\ListingEmpty
 */
class XpBlockListingEmpty extends AreaPluginBase {

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->accessManager = $container->get('access_manager');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      /** @var \Drupal\Core\Access\AccessResultInterface|\Drupal\Core\Cache\CacheableDependencyInterface $access_result */
      $access_result = $this->accessManager->checkNamedRoute('xp_block.add_page', [], $this->currentUser, TRUE);
      $element = [
        '#markup' => $this->t('Add an <a href=":url">XP block</a>.', [':url' => Url::fromRoute('xp_block.add_page')->toString()]),
        '#access' => $access_result->isAllowed(),
        '#cache' => [
          'contexts' => $access_result->getCacheContexts(),
          'tags' => $access_result->getCacheTags(),
          'max-age' => $access_result->getCacheMaxAge(),
        ],
      ];
      return $element;
    }
    return [];
  }

}
