<?php

namespace Drupal\xp_demo_block\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class XpDemoBlockRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Allow anyone to test the external personalization demo.
    // @see xp_demo_block_page_attachments()
    if ($route = $collection->get('xp.test')) {
      $route->setRequirements(['_access' => 'TRUE']);
    }
  }

}
