<?php

namespace Drupal\xp_demo_block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\xp\XpSetupInterface;
use Drupal\xp\XpSetupModuleInterface;

/**
 * External personalization demo block setup service.
 */
class XpDemoBlockSetup implements XpSetupModuleInterface {

  use StringTranslationTrait;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The XP setup service.
   *
   * @var \Drupal\xp\XpSetupInterface
   */
  protected $setup;

  /**
   * Constructs a XpDemoBlockSetup object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\xp\XpSetupInterface $xp_setup
   *   The XP setup service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, XpSetupInterface $xp_setup) {
    $this->configFactory = $config_factory;

    // Clone the XP setup service so that we can assign it a unique ID.
    // @todo Use factory pattern to XP demo service initialization.
    // @see https://api.drupal.org/api/drupal/core!core.api.php/group/container/8.2.x
    $this->setup = clone $xp_setup;
    $this->setup->setId('xp_demo_block');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $block_content_ids = [];
    $block_content_ids[] = $this->createBasicExample();
    $block_content_ids[] = $this->createBlockExample();
    $block_content_ids[] = $this->createParagraphExample();
    $this->createBlockTestForm($block_content_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function uninstall() {
    $this->setup->deleteEntities();
  }

  /* ************************************************************************ */
  // Demo block.
  /* ************************************************************************ */

  /**
   * Create demo block.
   */
  protected function createBlockTestForm(array $block_content_ids) {
    $this->setup->createEntity('block', [
      'plugin' => 'xp_block_test',
      'id' => 'bartik_xp_demo_test',
      'weight' => 100,
      'settings' => [
        'label' => $this->t('External Personalization Demo'),
        'block_content_ids' => array_combine($block_content_ids, $block_content_ids),
      ],
    ] + $this->getBlockConfiguration());
  }

  /* ************************************************************************ */
  // Basic example.
  /* ************************************************************************ */

  /**
   * Create basic example.
   */
  protected function createBasicExample() {
    $block_content = $this->setup->createEntity('block_content', [
      'info' => $this->t('Basic Example'),
      'type' => 'xp_basic',
      'field_xp_id' => ['value' => 'xp_basic_example'],
      'field_xp_notes' => ['value' => $this->t('This is an example of a basic personalized HTML block.')],
      'field_xp_variants' => $this->getBasicFieldVariants(),
    ]);
    $this->setup->createEntity('block', [
      'plugin' => 'block_content:' . $block_content->uuid(),
      'id' => 'bartik_xp_basic_example',
      'weight' => 101,
      'settings' => ['label' => $this->t('Basic Example')],
    ] + $this->getBlockConfiguration());
    return $block_content->id();
  }

  /**
   * Create variants body.
   *
   * @return array
   *   Variants HTML field values.
   */
  protected function getBasicFieldVariants() {
    $variants = [
      'x' => 'X',
      'a' => 'A',
      'b' => 'B',
      'c' => 'C',
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $values[] = Paragraph::create([
        'type' => 'xp_variant_body',
        'field_xp_variant_id' => ['value' => $id],
        'field_xp_variant_notes' => ['value' => $this->t('The letter @letter', ['@letter' => $text])],
        'field_xp_variant_body' => [
          'value' => '<h3>' . $text . '</h3>',
          'format' => 'basic_html',
        ],
      ]);
    }
    return $values;
  }

  /* ************************************************************************ */
  // External personalization block example.
  /* ************************************************************************ */

  /**
   * Create block field example.
   */
  protected function createBlockExample() {
    $block_content = $this->setup->createEntity('block_content', [
      'info' => $this->t('Block Example'),
      'type' => 'xp_block',
      'field_xp_id' => ['value' => 'xp_block_example'],
      'field_xp_notes' => ['value' => $this->t('This is an example of a personalized block with blocks.')],
      'field_xp_variants' => $this->getBlockFieldVariants(),
    ]);
    $this->setup->createEntity('block', [
      'plugin' => 'block_content:' . $block_content->uuid(),
      'id' => 'bartik_xp_block_example',
      'weight' => 102,
      'settings' => ['label' => $this->t('Block Example')],
    ] + $this->getBlockConfiguration());
    return $block_content->id();
  }

  /**
   * Create variants block field.
   *
   * @return array
   *   Variants block field values.
   */
  protected function getBlockFieldVariants() {
    $variants = [
      '0' => '0',
      '1' => '1',
      '2' => '2',
      '3' => '3',
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $t_args = ['@number' => $text];
      $block_content = $this->setup->createEntity('block_content', [
        'info' => $this->t('Block Example @number', $t_args),
        'type' => 'basic',
        'body' => [
          'value' => '<h3>' . $text . '</h3>',
          'format' => 'basic_html',
        ],
      ]);
      $plugin_id = 'block_content:' . $block_content->uuid();
      $values[] = Paragraph::create([
        'type' => 'xp_variant_block',
        'field_xp_variant_id' => [
          'value' => $id,
        ],
        'field_xp_variant_notes' => ['value' => $this->t('The number @number', $t_args)],
        'field_xp_variant_block' => [
          'plugin_id' => $plugin_id,
          'settings' => [
            'id' => $plugin_id,
            'label' => $text,
            'provider' => 'block_content',
            'label_display' => FALSE,
            'status' => TRUE,
            'info' => '',
            'view_mode' => 'full',
          ],
        ],
      ]);
    }
    return $values;
  }

  /* ************************************************************************ */
  // Paragraphs example.
  /* ************************************************************************ */

  /**
   * Create paragraphs example.
   */
  protected function createParagraphExample() {
    $block_content = $this->setup->createEntity('block_content', [
      'info' => $this->t('Paragraph Example'),
      'type' => 'xp_paragraph',
      'field_xp_id' => ['value' => 'xp_paragraph_example'],
      'field_xp_notes' => ['value' => $this->t('This is an example of a personalized block with paragraphs.')],
      'field_xp_options' => [
        ['value' => 'default'],
        ['value' => 'ajax'],
      ],
      'field_xp_variants' => $this->getParagraphFieldVariants(),
    ]);
    $this->setup->createEntity('block', [
      'plugin' => 'block_content:' . $block_content->uuid(),
      'id' => 'bartik_xp_paragraph_example',
      'weight' => 103,
      'settings' => ['label' => $this->t('Paragraph Example')],
    ] + $this->getBlockConfiguration());
    return $block_content->id();
  }

  /**
   * Create variants paragraph.
   *
   * @return array
   *   Variants paragraph field values.
   */
  protected function getParagraphFieldVariants() {
    $variants = [
      'kittens' => $this->t('I love cute kittens.'),
      'dog' => $this->t('Dogs rock!!!'),
      'bear' => $this->t('Bears are cuddly.'),
      'bacon' => $this->t('I eat bacon.'),
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $source = drupal_get_path('module', 'xp_demo_block') . '/images/' . $id . '.jpeg';
      $destination = 'public://xp_demo_block-' . $id . '.jpeg';
      $file = $this->setup->createFile($source, $destination);
      $values[] = Paragraph::create([
        'type' => 'xp_variant_paragraph',
        'field_xp_variant_id' => ['value' => $id],
        'field_xp_variant_notes' => ['value' => $text],
        'field_xp_variant_paragraph' => Paragraph::create([
          'type' => 'xp_demo_block',
          'field_xp_demo_block_image' => [
            'target_id' => $file->id(),
            'alt' => $id,
            'title' => $id,
          ],
          'field_xp_demo_block_text' => [
            'value' => $text,
            'format' => 'basic_html',
          ],
        ]),
      ]);
    }

    return $values;
  }

  /* ************************************************************************ */
  // Block helpers.
  /* ************************************************************************ */

  /**
   * Get the block default configuration for the default theme.
   *
   * @return string[]
   *   the block default configuration for the default theme, which is
   *   Bartik or Olivero.
   *
   * @throws \Exception
   *   Throw expection is the default theme is not Bartik or Olivero.
   */
  protected function getBlockConfiguration() {
    $default_theme = $this->configFactory->get('system.theme')->get('default');
    switch ($default_theme) {
      case 'bartik':
        return [
          'theme' => 'bartik',
          'region' => 'sidebar_second',
        ];

      case 'olivero':
        return [
          'theme' => 'olivero',
          'region' => 'sidebar',
        ];

      default:
        throw new \Exception('The External Personalization (XP) Block Demo module can be only installed with the Bartik or Olivero theme is set as the default theme.');
    }
  }

}
