<?php

namespace Drupal\Tests\xp_demo_block\Functional;

use Drupal\Tests\xp\Functional\XpBrowserTestBase;

/**
 * Test the xp_demo_block.module's installation and uninstallation.
 *
 * @group xp
 */
class XpDemoBlockInstallTest extends XpBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_demo_block'];

  /**
   * Tests the demo.
   */
  public function testDemo() {
    $session = $this->assertSession();

    // Check that xp/test is always included.
    $this->drupalGet('<front>');
    $session->responseContains('xp.test.js');
    $this->drupalGet('/xp/test');
    $session->statusCodeEquals(200);

    // Check .xp-demo-block class is added to demo blocks.
    // @see xp_demo_block_preprocess_block()
    $this->drupalGet('<front>');
    $session->elementExists('css', '#block-bartik-xp-basic-example.xp-demo-block');
    $session->elementExists('css', '#block-bartik-xp-block-example.xp-demo-block');
    $session->elementExists('css', '#block-bartik-xp-paragraph-example.xp-demo-block');

    // Check that the test form displays selected variants.
    // @see \Drupal\xp_block\Form\XpBlockTestForm
    // @todo [Drupal 9.1.x] Add WebAssert::urlQueryStringEquals.
    // @see https://www.drupal.org/node/3165286
    $edit = [
      'blocks[1]' => 'a',
      'blocks[6]' => '1',
      'blocks[7]' => 'dog',
    ];
    $this->drupalPostForm('<front>', $edit, 'Display variants');
    $this->assertOptionSelected('blocks[1]', 'a');
    $this->assertOptionSelected('blocks[6]', '1');
    $this->assertOptionSelected('blocks[7]', 'dog');
  }

  /**
   * Tests the xp_demo_block.module's installation and uninstallation.
   */
  public function testInstallUninstall() {
    $assert = $this->assertSession();

    // Check that external personalization demo blocks are created.
    $this->drupalGet('<front>');
    $assert->elementExists('css', '#block-bartik-xp-demo-test');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_block_example" data-xp-default>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseContains('<div class="xp" data-xp="block" data-xp-id="xp_paragraph_example" data-xp-default>');

    // Uninstall the xp_demo_block.module.
    \Drupal::service('module_installer')->uninstall(['xp_demo_block']);

    // Check that external personalization demo blocks are uninstalled.
    $this->drupalGet('<front>');
    $assert->responseNotContains('<div class="xp" data-xp="block" data-xp-id="xp_block_example" data-xp-default>');
    $assert->responseNotContains('<div class="xp" data-xp="block" data-xp-id="xp_basic_example" data-xp-default>');
    $assert->responseNotContains('<div class="xp" data-xp="block" data-xp-id="xp_paragraph_example" data-xp-default>');
  }

}
