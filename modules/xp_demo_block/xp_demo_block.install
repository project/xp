<?php

/**
 * @file
 * Install, update and uninstall functions for the External Personalization Demo module.
 */

/**
 * Implements hook_requirements().
 */
function xp_demo_block_requirements($phase) {
  $requirements = [];
  if ($phase === 'install') {
    $default_theme = \Drupal::config('system.theme')->get('default');
    if (!in_array($default_theme, ['bartik', 'olivero'])) {
      $requirements['xp_demo_block'] = [
        'title' => t('Default theme'),
        'description' => t('The External Personalization (XP) Block Demo module can be only installed with the Bartik or Olivero theme is set as the default theme.'),
        'severity' => REQUIREMENT_ERROR,
      ];
    }
  }
  return $requirements;
}

/**
 * Implements hook_install().
 */
function xp_demo_block_install() {
  // Require the Bartik or Olivero theme.
  // @see xp_demo_block_requirements)_
  $default_theme = \Drupal::config('system.theme')->get('default');
  if (!in_array($default_theme, ['bartik', 'olivero'])) {
    return;
  }

  /** @var \Drupal\xp\XpSetupModuleInterface $setup */
  $setup = \Drupal::service('xp_demo_block.setup');
  $setup->install();
}

/**
 * Implements hook_install().
 */
function xp_demo_block_uninstall() {
  /** @var \Drupal\xp\XpSetupModuleInterface $setup */
  $setup = \Drupal::service('xp_demo_block.setup');
  $setup->uninstall();
}
