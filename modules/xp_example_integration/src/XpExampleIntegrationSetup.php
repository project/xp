<?php

namespace Drupal\xp_example_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\xp\XpSetupInterface;
use Drupal\xp\XpSetupModuleInterface;

/**
 * External personalization example integration setup service.
 */
class XpExampleIntegrationSetup implements XpSetupModuleInterface {

  use StringTranslationTrait;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The XP setup service.
   *
   * @var \Drupal\xp\XpSetupInterface
   */
  protected $setup;

  /**
   * Constructs a XpExampleIntegrationSetup object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\xp\XpSetupInterface $xp_setup
   *   The XP setup service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, XpSetupInterface $xp_setup) {
    $this->configFactory = $config_factory;

    // Clone the XP setup service so that we can assign it a unique ID.
    // @todo Use factory pattern to XP demo service initialization.
    // @see https://api.drupal.org/api/drupal/core!core.api.php/group/container/8.2.x
    $this->setup = clone $xp_setup;
    $this->setup->setId('xp_example_integration');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->createBasicExample();
  }

  /**
   * {@inheritdoc}
   */
  public function uninstall() {
    $this->setup->deleteEntities();
  }

  /**
   * Create basic example.
   */
  protected function createBasicExample() {
    $block_content = $this->setup->createEntity('block_content', [
      'info' => $this->t('Example Integration'),
      'type' => 'xp_basic',
      'field_xp_id' => ['value' => 'xp_example_integration'],
      'field_xp_notes' => ['value' => $this->t('This is an example of a example integration block.')],
      'field_xp_variants' => $this->getFieldVariants(),
    ]);
    $this->setup->createEntity('block', [
      'plugin' => 'block_content:' . $block_content->uuid(),
      'id' => 'bartik_xp_example_integration',
      'settings' => ['label' => $this->t('Example integration')],
    ] + $this->getBlockConfiguration());
    return $block_content->id();
  }

  /**
   * Create variants body.
   *
   * @return array
   *   Variants HTML field values.
   */
  protected function getFieldVariants() {
    $variants = [
      'i' => 'I',
      'ii' => 'III',
      'iii' => 'III',
      'iv' => 'IV',
      'v' => 'V',
    ];
    $values = [];
    foreach ($variants as $id => $text) {
      $t_args = [
        ':href' => '<a href="https://en.wikipedia.org/wiki/Roman_numerals',
        '@numeral' => $text,
      ];
      $values[] = Paragraph::create([
        'type' => 'xp_variant_body',
        'field_xp_variant_id' => ['value' => $id],
        'field_xp_variant_notes' => ['value' => $this->t('The roman numeral @numeral', $t_args)],
        'field_xp_variant_body' => [
          'value' => '<blockquote>'
          . $this->t('The roman numeral <strong>@numeral</strong>', $t_args)
          . '<br />'
          . $this->t('<a href=":href">Learn more</a>', $t_args)
          . '</blockquote>',
          'format' => 'basic_html',
        ],
      ]);
    }
    return $values;
  }

  /* ************************************************************************ */
  // Block helpers.
  /* ************************************************************************ */

  /**
   * Get the block default configuration for the default theme.
   *
   * @return string[]
   *   the block default configuration for the default theme, which is
   *   Bartik or Olivero.
   *
   * @throws \Exception
   *   Throw expection is the default theme is not Bartik or Olivero.
   */
  protected function getBlockConfiguration() {
    $default_theme = $this->configFactory->get('system.theme')->get('default');
    switch ($default_theme) {
      case 'bartik':
        return [
          'theme' => 'bartik',
          'region' => 'sidebar_second',
        ];

      case 'olivero':
        return [
          'theme' => 'olivero',
          'region' => 'sidebar',
        ];

      default:
        throw new \Exception('The External Personalization (XP) Block Demo module can be only installed with the Bartik or Olivero theme is set as the default theme.');
    }
  }

}
