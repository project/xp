<?php

/**
 * @file
 * Provides an example for integrating an external personalization engine.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\xp_block\Form\XpBlockForm;
use Drupal\xp_block\Utility\XpBlockHelper;

/**
 * Implements hook_page_attachments().
 */
function xp_example_integration_page_attachments(array &$attachments) {
  if (!xp_example_integration_enabled()) {
    return;
  }

  $config = \Drupal::config('xp_example_integration.settings');

  // Add versioned global JavaScript library.
  $version = $config->get('version');
  $attachments['#attached']['library'][] = 'xp_example_integration/xp_example_integration.' . $version . '.page';

  // Add environment settings.
  $environment = $config->get('environment');
  $settings = $config->get('environments.' . $environment) ?: [];
  $attachments['#attached']['drupalSettings']['xp_example_integration'] = $settings;

  // Add config as cache dependency.
  /** @var \Drupal\Core\Render\RendererInterface $renderer */
  $renderer = \Drupal::service('renderer');
  $renderer->addCacheableDependency($attachments, $config);
}

/**
 * Implements hook_element_info_alter().
 */
function xp_example_integration_element_info_alter(array &$info) {
  if (!xp_example_integration_enabled()) {
    return;
  }

  $version = \Drupal::config('xp_example_integration.settings')->get('version');
  $info['xp']['#attached']['library'][] = 'xp_example_integration/xp_example_integration.' . $version . '.element';
}

/* *************************************************************************** */
// Personalize XP block form.
/* *************************************************************************** */

/**
 * Implements hook_form_alter().
 */
function xp_example_integration_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if (!$form_object instanceof XpBlockForm) {
    return;
  }

  $module_name = 'xp_example_integration';

  /** @var \Drupal\block_content\BlockContentInterface $block_content */
  $block_content = $form_object->getEntity();
  // Details.
  $form[$module_name] = [
    '#type' => 'details',
    '#title' => t('Example integration'),
    '#description' => '<p>' . t('Below is where you would display data pulled from your external personalization decision engine.') . '</p>',
  ];
  // Table.
  $header = [
    'id' => ['data' => t('Variant ID'), 'width' => '20%'],
    'details' => ['data' => t('Variant details'), 'width' => '80%'],
  ];
  $rows = [];
  $variants = XpBlockHelper::getVariants($block_content);
  foreach ($variants as $variant_id => $variant_item) {
    $row['id'] = $variant_id;
    $row['details'] = t("Some additional details about the '@variant' variant.", ['@variant' => $variant_id]);
    $rows[] = $row;
  }
  $form[$module_name]['table'] = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  ];
  // Actions.
  $form[$module_name]['actions'] = ['#type' => 'actions'];
  $form[$module_name]['actions']['submit'] = [
    '#type' => 'submit',
    '#button_type' => 'primary',
    '#value' => t('Edit decisions'),
    '#submit' => ['_xp_example_integration_submit'],
  ];
}

/**
 * Submit callback for example integration settings.
 */
function _xp_example_integration_submit(&$form, FormStateInterface $form_state) {
  \Drupal::messenger()->addStatus(t('{Redirect to external decision engine URL}'));
}

/* *************************************************************************** */
// Settings form.
/* *************************************************************************** */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function xp_example_integration_form_xp_settings_form_alter(&$form, FormStateInterface $form_state) {
  $module_name = 'xp_example_integration';
  $config = \Drupal::config($module_name . '.settings');

  $environments = [
    'development' => t('Development (development)'),
    'test' => t('Test (test)'),
    'production' => t('Production (production)'),
  ];

  /** @var Drupal\Core\Asset\LibraryDiscovery $library_discovery */
  $library_discovery = \Drupal::service('library.discovery');
  $libraries = $library_discovery->getLibrariesByExtension($module_name);
  $versions = [];
  foreach ($libraries as $library_name => $library_info) {
    $version = (string) $library_info['version'];
    if (strpos($library_name, $module_name . '.' . $version) === 0) {
      $versions[$version] = $version;
    }
  }

  $form[$module_name] = [
    '#type' => 'details',
    '#title' => t('Example Integration settings'),
    '#open' => TRUE,
    '#tree' => TRUE,
  ];
  $form[$module_name]['version'] = [
    '#type' => 'select',
    '#title' => t('Version'),
    '#options' => $versions,
    '#required' => TRUE,
    '#default_value' => $config->get('version'),
  ];
  $form[$module_name]['environment'] = [
    '#type' => 'select',
    '#title' => t('Environment'),
    '#options' => $environments,
    '#required' => TRUE,
    '#default_value' => $config->get('environment'),
  ];
  foreach ($environments as $environment_name => $environment_label) {
    $form[$module_name]['environments'][$environment_name] = [
      '#type' => 'details',
      '#title' => t('@environment settings', ['@environment' => $environment_label]),
    ];
    $environment_form =& $form[$module_name]['environments'][$environment_name];
    $environment_form['clientId'] = [
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('environments.' . $environment_name . '.clientId'),
    ];
    $environment_form['serverUrl'] = [
      '#type' => 'textfield',
      '#title' => t('Server URL'),
      '#required' => TRUE,
      '#default_value' => $config->get('environments.' . $environment_name . '.serverUrl'),
    ];
    $environment_form['debug'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable debugging'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('environments.' . $environment_name . '.debug'),
    ];
  }

  $form['#submit'][] = '_xp_example_integration_form_xp_settings_form_submit';
}

/**
 * Submit callback for example integration settings.
 */
function _xp_example_integration_form_xp_settings_form_submit(&$form, FormStateInterface $form_state) {
  $module_name = 'xp_example_integration';
  $config = \Drupal::configFactory()->getEditable($module_name . '.settings');
  $data = $form_state->getValue($module_name);
  foreach ($data['environments'] as &$environment) {
    $environment['debug'] = (boolean) $environment['debug'];
  }
  $config->setData($data);
  $config->save();
}

/* *************************************************************************** */
// Helper functions.
/* *************************************************************************** */

/**
 * Determine if the XP Example Integration is enabled.
 *
 * XP Example Integration is on enabled if the XP status is TRUE and the user
 * is NOT on admin route.
 *
 * @return bool
 *   TRUE if the XP Example Integration is enabled.
 */
function xp_example_integration_enabled() {
  return (\Drupal::config('xp.settings')->get('status')
    && !\Drupal::service('router.admin_context')->isAdminRoute());
}
