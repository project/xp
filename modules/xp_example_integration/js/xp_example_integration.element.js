/**
 * Code which is added when an 'xp' element is being displayed.
 */

/* eslint-disable  no-undef */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.xpExampleIntegration = {
    attach: function (context) {
      var items = Drupal.Xp.getItems(context);
      XpExampleIntegration.triggerDecisions(items);
    }
  };

}(jQuery, Drupal, drupalSettings));
