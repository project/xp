/**
 * Tracking and sdk code which is added to every page.
 */

/* eslint-disable  no-undef */

(function ($, Drupal) {

  'use strict';

  /**
   * Global XpExampleIntegration object.
   *
   * @type {object}
   */
  window.XpExampleIntegration = {};

  // Create a

  /**
   * Mock dataLayer for Google Tag Manager.
   *
   * @type {array}
   *
   * @see https://developers.google.com/tag-platform/tag-manager/web/datalayer
   */
  var dataLayer = window.dataLayer = window.dataLayer || [];

  /**
   * Console styling.
   *
   * @type {string}
   */
  var consoleStyle = 'background: #90ee90; color: #ooo; padding: 5px';

  /**
   * Get decisions and display variants.
   *
   * @param {Array} items
   *   An array of item objects which contain type, and id properties.
   */
  XpExampleIntegration.triggerDecisions = function (items) {
    // Below is just some basic debugging and testing code.
    items.forEach(function (item) {
      console.log('%c Found EXAMPLE: ' + Object.values(item).join('/'), consoleStyle);

      Drupal.Xp.getVariantsIds(item).forEach(function (variant) {
        var url = window.location.href.split('?')[0];
        console.log('%c Test EXAMPLE \'' + variant + '\' variant: ' + url + '?_xp_example=' + encodeURIComponent(variant), consoleStyle);
      });
    });

    // Get the current variant id from the query string.
    var currentVariantId = window.location.search.replace('?_xp_example=', '');

    // Loop through all the items.
    //
    // In a real world situation you would call your external personalization
    // decision engine's API with the item data and get back the
    // variant as a decision.
    items.forEach(function (item) {

      // Loop thru all item variants.
      Drupal.Xp.getVariantsIds(item).forEach(function (variant) {
        // If we find a matching variant id, show this variant and push to the
        // date layer.
        if (variant === currentVariantId) {
          item.variant = variant;
          console.log('%c Displaying EXAMPLE variant: ' + Object.values(item).join('/'), consoleStyle);
          Drupal.Xp.showVariant(item, function showVariantCallback() {
            dataLayerPush(item);
          });
        }
      });
    });
  };

  /**
   * Push XP item to the data layer and append the item to all hrefs.
   *
   * @param {object} item
   *   An XP item.
   */
  function dataLayerPush(item) {
    var xpPath = Object.values(item).join('/');

    // Push xp path and item to the data layer.
    var data = {'xp': xpPath, 'item': item};
    dataLayer.push(data);
    console.log('%c Pushing EXAMPLE variant to dataLayer: ' + JSON.stringify(data), consoleStyle);

    // Append ?context_path= to all hrefs inside the variant element.
    var query = $.param({'xp': xpPath});
    Drupal.Xp.getVariantElement(item)
      .find('a[href]')
      .each(function appendContextPath() {
        var $a = $(this);
        var href = $a.attr('href');
        if (href.indexOf('xp=') === -1) {
          href += (href.match(/\?/) ? '&' : '?') + query;
          $a.attr('href', href);
        }
      });
  }

}(jQuery, Drupal));
