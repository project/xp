<?php

namespace Drupal\Tests\xp_example_integration\Functional;

use Drupal\Tests\xp_block\Functional\XpBlockTestBase;

/**
 * Test the XP Example Integration module.
 *
 * @group xp
 */
class XpExampleIntegrationTest extends XpBlockTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'bartik';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['xp_example_integration'];

  /**
   * Tests the XP Example Integration module.
   */
  public function testXpExampleIntegration() {
    $assert = $this->assertSession();

    // Check that js and settings are added to external page.
    $this->drupalGet('<front>');
    $assert->responseContains('xp_example_integration.page.js?v=1.0.0');
    $assert->responseContains('"xp_example_integration":{"clientId":"f91021b0-26bf-11ec-9621-0242ac130002","serverUrl":"dev.example_integration.com","debug":false}');

    // Check that page js and settings are NOT added to admin pages.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/people');
    $assert->responseNotContains('xp_example_integration.page.js?v=1.0.0');
    $assert->responseNotContains('"xp_example_integration":{"clientId":"f91021b0-26bf-11ec-9621-0242ac130002","serverUrl":"dev.example_integration.com","debug":false}');
    $this->drupalLogout();

    // Create XP basic block.
    $this->createXpBasicBlock();

    // Check that element js is added to external page.
    $this->drupalGet('<front>');
    $assert->responseContains('xp_example_integration.element.js?v=1.0.0');

    // Change version and environment.
    $this->drupalLogin($this->rootUser);
    $edit = [
      'xp_example_integration[version]' => '2.0.0',
      'xp_example_integration[environment]' => 'test',
      'xp_example_integration[environments][test][debug]' => TRUE,
    ];
    $this->drupalPostForm('/admin/config/content/xp', $edit, 'Save configuration');
    $this->drupalLogout();

    // Check that js and settings are updated to version 2.0.0.
    $this->drupalGet('<front>');
    $assert->responseNotContains('xp_example_integration.page.js?v=1.0.0');
    $assert->responseNotContains('"xp_example_integration":{"clientId":"f91021b0-26bf-11ec-9621-0242ac130002","serverUrl":"dev.example_integration.com","debug":false}');
    $assert->responseContains('xp_example_integration.page.js?v=2.0.0');
    $assert->responseContains('"xp_example_integration":{"clientId":"e048bab0-26c0-11ec-9621-0242ac130002","serverUrl":"test.example_integration.com","debug":true}');
  }

}
